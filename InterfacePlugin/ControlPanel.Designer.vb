﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmControlPanel
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmControlPanel))
        Me.grb_NETOutput1 = New System.Windows.Forms.GroupBox()
        Me.trk_WashoutPower = New System.Windows.Forms.TrackBar()
        Me.trk_Speed = New System.Windows.Forms.TrackBar()
        Me.lbl_Status = New System.Windows.Forms.Label()
        Me.btn_Recenter = New System.Windows.Forms.Button()
        Me.btn_Stop = New System.Windows.Forms.Button()
        Me.lbl_Config2 = New System.Windows.Forms.Label()
        Me.lbl_Config1 = New System.Windows.Forms.Label()
        Me.btn_Start = New System.Windows.Forms.Button()
        Me.lbl_title = New System.Windows.Forms.Label()
        Me.grb_NETOutput1.SuspendLayout()
        CType(Me.trk_WashoutPower, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trk_Speed, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grb_NETOutput1
        '
        Me.grb_NETOutput1.BackColor = System.Drawing.Color.Transparent
        Me.grb_NETOutput1.BackgroundImage = CType(resources.GetObject("grb_NETOutput1.BackgroundImage"), System.Drawing.Image)
        Me.grb_NETOutput1.Controls.Add(Me.trk_WashoutPower)
        Me.grb_NETOutput1.Controls.Add(Me.trk_Speed)
        Me.grb_NETOutput1.Controls.Add(Me.lbl_Status)
        Me.grb_NETOutput1.Controls.Add(Me.btn_Recenter)
        Me.grb_NETOutput1.Controls.Add(Me.btn_Stop)
        Me.grb_NETOutput1.Controls.Add(Me.lbl_Config2)
        Me.grb_NETOutput1.Controls.Add(Me.lbl_Config1)
        Me.grb_NETOutput1.Controls.Add(Me.btn_Start)
        Me.grb_NETOutput1.Controls.Add(Me.lbl_title)
        Me.grb_NETOutput1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.grb_NETOutput1.ForeColor = System.Drawing.SystemColors.Control
        Me.grb_NETOutput1.Location = New System.Drawing.Point(1, 1)
        Me.grb_NETOutput1.Name = "grb_NETOutput1"
        Me.grb_NETOutput1.Size = New System.Drawing.Size(523, 124)
        Me.grb_NETOutput1.TabIndex = 274
        Me.grb_NETOutput1.TabStop = False
        Me.grb_NETOutput1.Text = "YawVR Control Panel"
        '
        'trk_WashoutPower
        '
        Me.trk_WashoutPower.Location = New System.Drawing.Point(283, 14)
        Me.trk_WashoutPower.Maximum = 5
        Me.trk_WashoutPower.Minimum = -5
        Me.trk_WashoutPower.Name = "trk_WashoutPower"
        Me.trk_WashoutPower.Size = New System.Drawing.Size(105, 45)
        Me.trk_WashoutPower.TabIndex = 308
        Me.trk_WashoutPower.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        Me.trk_WashoutPower.Visible = False
        '
        'trk_Speed
        '
        Me.trk_Speed.Location = New System.Drawing.Point(395, 14)
        Me.trk_Speed.Minimum = -10
        Me.trk_Speed.Name = "trk_Speed"
        Me.trk_Speed.Size = New System.Drawing.Size(105, 45)
        Me.trk_Speed.TabIndex = 307
        Me.trk_Speed.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        Me.trk_Speed.Visible = False
        '
        'lbl_Status
        '
        Me.lbl_Status.AutoSize = True
        Me.lbl_Status.BackColor = System.Drawing.Color.Transparent
        Me.lbl_Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Status.Location = New System.Drawing.Point(20, 57)
        Me.lbl_Status.Name = "lbl_Status"
        Me.lbl_Status.Size = New System.Drawing.Size(20, 15)
        Me.lbl_Status.TabIndex = 300
        Me.lbl_Status.Text = "ok"
        '
        'btn_Recenter
        '
        Me.btn_Recenter.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn_Recenter.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Recenter.ForeColor = System.Drawing.Color.Black
        Me.btn_Recenter.Location = New System.Drawing.Point(372, 78)
        Me.btn_Recenter.Name = "btn_Recenter"
        Me.btn_Recenter.Size = New System.Drawing.Size(128, 35)
        Me.btn_Recenter.TabIndex = 299
        Me.btn_Recenter.Text = "Recenter"
        Me.btn_Recenter.UseVisualStyleBackColor = False
        '
        'btn_Stop
        '
        Me.btn_Stop.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn_Stop.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Stop.ForeColor = System.Drawing.Color.Black
        Me.btn_Stop.Location = New System.Drawing.Point(157, 78)
        Me.btn_Stop.Name = "btn_Stop"
        Me.btn_Stop.Size = New System.Drawing.Size(128, 35)
        Me.btn_Stop.TabIndex = 298
        Me.btn_Stop.Text = "Stop"
        Me.btn_Stop.UseVisualStyleBackColor = False
        '
        'lbl_Config2
        '
        Me.lbl_Config2.AutoSize = True
        Me.lbl_Config2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_Config2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Config2.Location = New System.Drawing.Point(140, 34)
        Me.lbl_Config2.Name = "lbl_Config2"
        Me.lbl_Config2.Size = New System.Drawing.Size(86, 18)
        Me.lbl_Config2.TabIndex = 297
        Me.lbl_Config2.Text = "not active."
        '
        'lbl_Config1
        '
        Me.lbl_Config1.AutoSize = True
        Me.lbl_Config1.BackColor = System.Drawing.Color.Transparent
        Me.lbl_Config1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Config1.Location = New System.Drawing.Point(140, 16)
        Me.lbl_Config1.Name = "lbl_Config1"
        Me.lbl_Config1.Size = New System.Drawing.Size(73, 18)
        Me.lbl_Config1.TabIndex = 296
        Me.lbl_Config1.Text = "Interface"
        '
        'btn_Start
        '
        Me.btn_Start.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn_Start.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Start.ForeColor = System.Drawing.Color.Black
        Me.btn_Start.Location = New System.Drawing.Point(23, 78)
        Me.btn_Start.Name = "btn_Start"
        Me.btn_Start.Size = New System.Drawing.Size(128, 35)
        Me.btn_Start.TabIndex = 295
        Me.btn_Start.Text = "Start"
        Me.btn_Start.UseVisualStyleBackColor = False
        '
        'lbl_title
        '
        Me.lbl_title.AutoSize = True
        Me.lbl_title.BackColor = System.Drawing.Color.Transparent
        Me.lbl_title.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_title.Location = New System.Drawing.Point(20, 16)
        Me.lbl_title.Name = "lbl_title"
        Me.lbl_title.Size = New System.Drawing.Size(114, 18)
        Me.lbl_title.TabIndex = 267
        Me.lbl_title.Text = "Current limits:"
        '
        'frmControlPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(526, 127)
        Me.Controls.Add(Me.grb_NETOutput1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmControlPanel"
        Me.Text = "SimTools"
        Me.grb_NETOutput1.ResumeLayout(False)
        Me.grb_NETOutput1.PerformLayout()
        CType(Me.trk_WashoutPower, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trk_Speed, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents grb_NETOutput1 As Windows.Forms.GroupBox
    Friend WithEvents btn_Start As Windows.Forms.Button
    Friend WithEvents lbl_title As Windows.Forms.Label
    Friend WithEvents lbl_Config1 As Windows.Forms.Label
    Friend WithEvents btn_Recenter As Windows.Forms.Button
    Friend WithEvents btn_Stop As Windows.Forms.Button
    Friend WithEvents lbl_Config2 As Windows.Forms.Label
    Friend WithEvents lbl_Status As Windows.Forms.Label
    Friend WithEvents trk_WashoutPower As Windows.Forms.TrackBar
    Friend WithEvents trk_Speed As Windows.Forms.TrackBar
End Class
