﻿Option Strict On
Option Explicit On

Imports System.IO
Imports System.Xml.Serialization

Public Class UserControl_Settings
    '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    '///                 Edit the Subroutines below to provide support for your favorite Interface!                        ///
    '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    'Game Engine uses this to (Load the Form from the Structure)

    Public _ControlPanel As New frmControlPanel

    Private Sub LoadFormFromStructure()
        'Load Form from Structure
        txt_IPAddress1.Text = CStr(_InterfaceSettings._IPAddress1)
        txt_IPAddress2.Text = CStr(_InterfaceSettings._IPAddress2)
        txt_IPAddress3.Text = CStr(_InterfaceSettings._IPAddress3)
        txt_IPAddress4.Text = CStr(_InterfaceSettings._IPAddress4)
        txt_Port.Text = CStr(_InterfaceSettings._Port)
        chk_WriteMmf.CheckState = CType(_InterfaceSettings._WriteMmf, Windows.Forms.CheckState)
        chk_AutoOpenPanel.CheckState = CType(_InterfaceSettings._AutoOpenPanel, Windows.Forms.CheckState)

        cb_OutputBits.SelectedItem = "12"
        rad_Decimal.Checked = True
        cb_StartMS.SelectedItem = 100
        cb_StopMS.SelectedItem = 100
        cb_PacketRate.SelectedItem = 12
        txt_StartUpOutput.Text = ""
        txt_InterfaceOutput.Text = _InterfaceSettings._InterfaceOutput.ToString
        txt_InterfaceOutput.Text = "YS[<Axis1a>]P[<Axis2a>]R[<Axis3a>]B[12]"
        txt_ShutDownOutput.Text = _InterfaceSettings._ShutdownOutput.ToString
        txt_ShutDownOutput.Text = ""
        cbo_YawAxisMode.SelectedIndex = _InterfaceSettings._YawAxisMode
        Chk_Washout.CheckState = CType(_InterfaceSettings._WashOut, Windows.Forms.CheckState)
        trk_Speed.Value = _InterfaceSettings._Speed
        trk_WashoutPower.Value = _InterfaceSettings._Washoutpower


        PopulateDevices()

        If _InterfaceSettings._AutoOpenPanel = 1 Then
            btn_OpenPanel_Click(Me, Nothing)
            _ControlPanel.lbl_title.Text = "Active device:"
            _ControlPanel.lbl_Config1.Text = cbo_Devices.Text
            _ControlPanel.lbl_Config2.Text = "Interface stopped."
        End If

    End Sub

    'Game Engine uses this to (Load the Structure from the Form)
    Public Sub LoadStrutureFromForm()
        'Load Structure from Form
        'IP Address
        _InterfaceSettings._IPAddress1 = txt_IPAddress1.Text
        _InterfaceSettings._IPAddress2 = txt_IPAddress2.Text
        _InterfaceSettings._IPAddress3 = txt_IPAddress3.Text
        _InterfaceSettings._IPAddress4 = txt_IPAddress4.Text
        'PORT
        _InterfaceSettings._Port = "50010"
        _InterfaceSettings._OutputBits = "1"
        _InterfaceSettings._OutputType = "Decimal"
        _InterfaceSettings._StartupOutput = ""
        _InterfaceSettings._HWStartMS = 100
        _InterfaceSettings._InterfaceOutput = "YS[<Axis1a>]P[<Axis2a>]R[<Axis3a>]B[12]"
        _InterfaceSettings._OutPutRateMS = 12
        _InterfaceSettings._ShutdownOutput = ""
        _InterfaceSettings._HWStopMS = 100
        _InterfaceSettings._WriteMmf = chk_WriteMmf.CheckState
        _InterfaceSettings._AutoOpenPanel = chk_AutoOpenPanel.CheckState
        _InterfaceSettings._DeviceName = cbo_Devices.Text
        _InterfaceSettings._YawAxisMode = cbo_YawAxisMode.SelectedIndex
        _InterfaceSettings._WashOut = Chk_Washout.CheckState
        _InterfaceSettings._Speed = trk_Speed.Value
        _InterfaceSettings._Washoutpower = trk_WashoutPower.Value


    End Sub

    'Game Engine uses this to (Clear the Form)
    Public Sub ClearSettingsWindow()
        txt_IPAddress1.Text = ""
        txt_IPAddress2.Text = ""
        txt_IPAddress3.Text = ""
        txt_IPAddress4.Text = ""
        txt_Port.Text = "50010"
        cb_OutputBits.SelectedItem = "1"
        rad_Binary.Checked = True
        txt_StartUpOutput.Text = ""
        cb_StartMS.SelectedItem = "100"
        txt_InterfaceOutput.Text = "YS[<Axis1a>]P[<Axis2a>]R[<Axis3a>]B[12]"
        cb_PacketRate.SelectedItem = "12"
        txt_ShutDownOutput.Text = ""
        cb_StopMS.SelectedItem = "100"
        chk_WriteMmf.CheckState = Windows.Forms.CheckState.Checked
        chk_AutoOpenPanel.CheckState = Windows.Forms.CheckState.Checked
        cbo_YawAxisMode.SelectedIndex = 0
        Chk_Washout.CheckState = 0
        trk_Speed.Value = 0
        trk_WashoutPower.Value = 0

        PopulateDevices()

        If _InterfaceSettings._AutoOpenPanel = 1 Then
            btn_OpenPanel_Click(Me, Nothing)
        End If

    End Sub

    'Your form uses this to determine when the form has enough info to enable the 'Save' button
    Public Sub CheckSaveButton()
        Try
            'Can the User Save the Current Settings?
            If txt_IPAddress1.Text <> "" And txt_IPAddress2.Text <> "" And txt_IPAddress3.Text <> "" And txt_IPAddress4.Text <> "" Then
                btn_Save.Enabled = True
            Else
                btn_Save.Enabled = False
            End If
            If cbo_Devices.SelectedItem.ToString.Contains("not found") Then
                btn_Save.Enabled = False
            End If
        Catch ex As Exception
            btn_Save.Enabled = False
        End Try
    End Sub

    '////////////////////////////////////////////////////////////////////////////////////////
    '///               Edit the User Controls for the Settings Form Here                  ///
    '////////////////////////////////////////////////////////////////////////////////////////
    Private Sub txt_IPAddress1_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txt_IPAddress1.KeyPress
        Select Case e.KeyChar
            Case "0"c To "9"c
            Case " "c
                e.Handled = True ' Disallow
            Case Else
                ' Allow Tab, Enter , Cntrl+C etc
                If Convert.ToByte(e.KeyChar) > 32 Then
                    e.Handled = True ' Disallow
                End If
        End Select
    End Sub

    Private Sub txt_IPAddress2_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txt_IPAddress2.KeyPress
        Select Case e.KeyChar
            Case "0"c To "9"c
            Case " "c
                e.Handled = True ' Disallow
            Case Else
                ' Allow Tab, Enter , Cntrl+C etc
                If Convert.ToByte(e.KeyChar) > 32 Then
                    e.Handled = True ' Disallow
                End If
        End Select
    End Sub

    Private Sub txt_IPAddress3_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txt_IPAddress3.KeyPress
        Select Case e.KeyChar
            Case "0"c To "9"c
            Case " "c
                e.Handled = True ' Disallow
            Case Else
                ' Allow Tab, Enter , Cntrl+C etc
                If Convert.ToByte(e.KeyChar) > 32 Then
                    e.Handled = True ' Disallow
                End If
        End Select
    End Sub

    Private Sub txt_IPAddress4_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txt_IPAddress4.KeyPress
        Select Case e.KeyChar
            Case "0"c To "9"c
            Case " "c
                e.Handled = True ' Disallow
            Case Else
                ' Allow Tab, Enter , Cntrl+C etc
                If Convert.ToByte(e.KeyChar) > 32 Then
                    e.Handled = True ' Disallow
                End If
        End Select
    End Sub

    Private Sub txt_IPAddress1_TextChanged(sender As System.Object, e As System.EventArgs) Handles txt_IPAddress1.TextChanged
        Try
            If CInt(txt_IPAddress1.Text) > 255 Then
                txt_IPAddress1.Text = Mid(txt_IPAddress1.Text, 1, txt_IPAddress1.Text.Length - 1)
                txt_IPAddress1.SelectionStart = txt_IPAddress1.Text.Length
            End If
        Catch ex As Exception
            txt_IPAddress1.Text = ""
        End Try
        CheckSaveButton()
    End Sub

    Private Sub txt_IPAddress2_TextChanged(sender As System.Object, e As System.EventArgs) Handles txt_IPAddress2.TextChanged
        Try
            If CInt(txt_IPAddress2.Text) > 255 Then
                txt_IPAddress2.Text = Mid(txt_IPAddress2.Text, 1, txt_IPAddress2.Text.Length - 1)
                txt_IPAddress2.SelectionStart = txt_IPAddress2.Text.Length
            End If
        Catch ex As Exception
            txt_IPAddress2.Text = ""
        End Try
        CheckSaveButton()
    End Sub

    Private Sub txt_IPAddress3_TextChanged(sender As System.Object, e As System.EventArgs) Handles txt_IPAddress3.TextChanged
        Try
            If CInt(txt_IPAddress3.Text) > 255 Then
                txt_IPAddress3.Text = Mid(txt_IPAddress3.Text, 1, txt_IPAddress3.Text.Length - 1)
                txt_IPAddress3.SelectionStart = txt_IPAddress3.Text.Length
            End If
        Catch ex As Exception
            txt_IPAddress3.Text = ""
        End Try
        CheckSaveButton()
    End Sub

    Private Sub txt_IPAddress4_TextChanged(sender As System.Object, e As System.EventArgs) Handles txt_IPAddress4.TextChanged
        Try
            If CInt(txt_IPAddress4.Text) > 255 Then
                txt_IPAddress4.Text = Mid(txt_IPAddress4.Text, 1, txt_IPAddress4.Text.Length - 1)
                txt_IPAddress4.SelectionStart = txt_IPAddress4.Text.Length
            End If
        Catch ex As Exception
            txt_IPAddress4.Text = ""
        End Try
        CheckSaveButton()
    End Sub

    Private Sub txt_Port_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Port.KeyPress
        Select Case e.KeyChar
            Case "0"c To "9"c
            Case " "c
                e.Handled = True ' Disallow
            Case Else
                ' Allow Tab, Enter , Cntrl+C etc
                If Convert.ToByte(e.KeyChar) > 32 Then
                    e.Handled = True ' Disallow
                End If
        End Select
    End Sub

    Private Sub txt_Port_TextChanged(sender As System.Object, e As System.EventArgs) Handles txt_Port.TextChanged
        Try
            If CInt(txt_Port.Text) > 65535 Then
                txt_Port.Text = Mid(txt_Port.Text, 1, txt_Port.Text.Length - 1)
                txt_Port.SelectionStart = txt_Port.Text.Length
            End If
        Catch ex As Exception
            txt_Port.Text = ""
        End Try
        CheckSaveButton()
    End Sub

    Private Sub cb_OutputBits_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cb_OutputBits.SelectedIndexChanged
        CheckSaveButton()
    End Sub

    Private Sub cb_PacketRate_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cb_PacketRate.SelectedIndexChanged
        CheckSaveButton()
    End Sub

    Private Sub txt_InterfaceOutput_TextChanged(sender As System.Object, e As System.EventArgs) Handles txt_InterfaceOutput.TextChanged
        CheckSaveButton()
    End Sub

    Private Sub btn_OpenPanel_Click(sender As Object, e As EventArgs) Handles btn_OpenPanel.Click

        Dim frmCollection = System.Windows.Forms.Application.OpenForms
        If frmCollection.OfType(Of frmControlPanel).Any Then
            _ControlPanel.Show()
            _ControlPanel.Activate()
        Else
            '_ControlPanel = New frmControlPanel
            Dim IpAddress As String = _InterfaceSettings._IPAddress1 & "." & _InterfaceSettings._IPAddress2 & "." & _InterfaceSettings._IPAddress3 & "." & _InterfaceSettings._IPAddress4
            _ControlPanel.myIPAddress = IpAddress
            _ControlPanel.btn_Start.Enabled = False
            _ControlPanel.btn_Stop.Enabled = False
            _ControlPanel.btn_Recenter.Enabled = True
            _ControlPanel.Show()

        End If

    End Sub

    Private Sub btn_Rescan_Click(sender As Object, e As EventArgs) Handles btn_Rescan.Click

        PopulateDevices()

    End Sub

    Private Sub PopulateDevices()

        Try
            FindDevices()
            System.Threading.Thread.Sleep(200) ' We have to wait to be sure the devices had the time to respond

            cbo_Devices.Items.Clear()

            Dim strCompareIPAddress As String = ""

            If Not IsNothing(_InterfaceSettings._DeviceName) Then
                If InStr(_InterfaceSettings._DeviceName.ToString, "No device") = 0 Then
                    ' There is a saved device in the config file. Add "(not found) if not found on the network
                    strCompareIPAddress = _InterfaceSettings._IPAddress1 & "." & _InterfaceSettings._IPAddress2 & "." &
                    _InterfaceSettings._IPAddress3 & "." & _InterfaceSettings._IPAddress4

                    ' Is the saved config in the list of found devices?
                    Dim isSavedDeviceOnline As Boolean = False
                    For Each dev As YawDevice In YawDevices
                        If dev.DisplayName <> "" And dev.IpAddress = strCompareIPAddress Then
                            cbo_Devices.Items.Add(dev.DisplayName)
                            isSavedDeviceOnline = True
                        End If
                    Next
                    If isSavedDeviceOnline = False Then
                        cbo_Devices.Items.Add(_InterfaceSettings._DeviceName & " not found")
                    End If
                End If
            End If

            ' Populate the rest of the devices
            For Each dev As YawDevice In YawDevices
                If dev.DisplayName <> "" And dev.IpAddress <> strCompareIPAddress Then
                    cbo_Devices.Items.Add(dev.DisplayName)
                End If
            Next

            If cbo_Devices.Items.Count = 0 Then
                cbo_Devices.Items.Add("No device found")
            End If
            cbo_Devices.Items.Add("Manually enter IP address")

            cbo_Devices.SelectedIndex = 0
            cbo_Devices_SelectedIndexChanged(Me, Nothing)

        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation)
        End Try

    End Sub

    Private Sub cbo_Devices_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo_Devices.SelectedIndexChanged

        Try

            If InStr(cbo_Devices.SelectedItem.ToString, "No device") > 0 Then
                txt_IPAddress1.Text = ""
                txt_IPAddress2.Text = ""
                txt_IPAddress3.Text = ""
                txt_IPAddress4.Text = ""
            ElseIf InStr(cbo_Devices.SelectedItem.ToString, "Manually") > 0 Then
                Try
                    Dim response As String
                    Dim SplitIpAddress() As String
                    response = InputBox("Enter a valid IP address:", "YawVR driver", "")
                    SplitIpAddress = Split(response, ".")
                    txt_IPAddress1.Text = SplitIpAddress(0)
                    txt_IPAddress2.Text = SplitIpAddress(1)
                    txt_IPAddress3.Text = SplitIpAddress(2)
                    txt_IPAddress4.Text = SplitIpAddress(3)
                    cbo_Devices.Items.Add("Manual (" & SplitIpAddress(0) & "." & SplitIpAddress(1) & "." & SplitIpAddress(2) & "." & SplitIpAddress(3) & ")")
                    cbo_Devices.SelectedIndex = cbo_Devices.Items.Count - 1
                Catch ex As Exception
                    MsgBox("Error: IP address not valid", MsgBoxStyle.Exclamation, "YawVR driver")
                    cbo_Devices.SelectedIndex = 0
                    cbo_Devices_SelectedIndexChanged(Me, Nothing)
                    Exit Sub
                End Try
            ElseIf InStr(cbo_Devices.SelectedItem.ToString, "not found") = 0 Then
                For Each dev As YawDevice In YawDevices
                    If cbo_Devices.Text = dev.DisplayName Then
                        Dim SplitIpAddress() As String
                        SplitIpAddress = Split(dev.IpAddress, ".")
                        txt_IPAddress1.Text = SplitIpAddress(0)
                        txt_IPAddress2.Text = SplitIpAddress(1)
                        txt_IPAddress3.Text = SplitIpAddress(2)
                        txt_IPAddress4.Text = SplitIpAddress(3)
                    End If
                Next
            End If
            CheckSaveButton()

            _ControlPanel.lbl_title.Text = "Active device:"
            _ControlPanel.lbl_Config1.Text = cbo_Devices.Text
            _ControlPanel.lbl_Config2.Text = "Interface stopped."

        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation)
        End Try

    End Sub


    '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    '///                                                        DO NOT EDIT BELOW HERE!!!                                                           ///
    '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#Region "No Change Needed - works for all plugins!"
    'Our Settings Structure - used to hold all editable user settings (no change needed)
    Public _InterfaceSettings As New InterfacePlugin.InterfaceAssignments
    Public _InterfaceNumber As Integer 'SimTools sets this when it loads the dll
    Public _HasInitialized As Boolean = False 'SimTools sets this when it loads the dll  

    'default save location - (no change needed)
    Private _SavePath As String = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) & "\SimTools\GameEngine\"

    'Write the interface settings to a file
    Private Sub Save_InterfaceAssignments(FileName As String)
        LoadStrutureFromForm()

        'Write file
        Dim objWriter As New System.IO.StreamWriter(FileName, False)
        objWriter.WriteLine(SerializeInterfaceAssignments())
        objWriter.Close()
    End Sub

    'Create Preset folder when loaded
    Private Sub UserControl_Settings_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'create preset folder if it does not exist
        Dim FileName As String = _SavePath & "InterfacePresets\" & InterfacePlugin._InterfaceName.Replace(" ", "") & "_Presets"
        If System.IO.Directory.Exists(FileName) = False Then
            System.IO.Directory.CreateDirectory(FileName)
        End If

        'create Interface Plugin folder if it does not exist
        FileName = _SavePath & "Interface" & _InterfaceNumber & "\" & InterfacePlugin._InterfaceName.Replace(" ", "")
        If System.IO.Directory.Exists(FileName) = False Then
            System.IO.Directory.CreateDirectory(FileName)
        End If
    End Sub

    'Save a new/edited Preset
    Public Sub SaveThisPreset(Name As String)
        Dim FileName As String = _SavePath & "InterfacePresets\" & InterfacePlugin._InterfaceName.Replace(" ", "") & "_Presets\" & Name & "_" & InterfacePlugin._InterfaceName.Replace(" ", "") & "_InterfaceAssignments.pre"
        Save_InterfaceAssignments(FileName)
    End Sub

    'Serialize to string
    Private Function SerializeInterfaceAssignments() As String
        Dim string_writer As New StringWriter
        Dim SerializedOutput As String
        Dim xml_serializer As New XmlSerializer(GetType(InterfacePlugin.InterfaceAssignments))
        xml_serializer.Serialize(string_writer, _InterfaceSettings)
        SerializedOutput = string_writer.ToString()
        string_writer.Close()
        Return SerializedOutput
    End Function

    'De-Serialize from string
    Private Function DeSerializeInterfaceAssignments(InputString As String) As Object
        Dim xml_serializer As New XmlSerializer(GetType(InterfacePlugin.InterfaceAssignments))
        Dim string_reader As New StringReader(InputString)
        Dim DeserializedOutput As InterfacePlugin.InterfaceAssignments = DirectCast(xml_serializer.Deserialize(string_reader), InterfacePlugin.InterfaceAssignments)
        string_reader.Close()
        Return DeserializedOutput
    End Function

    'Save Settings
    Private Sub btn_Save_InterfaceSettings_Click(sender As System.Object, e As System.EventArgs) Handles btn_Save.Click
        'File Path
        Dim File_SavePath As String = _SavePath & "Interface" & _InterfaceNumber & "\" & InterfacePlugin._InterfaceName.Replace(" ", "") & "\" & InterfacePlugin._InterfaceName.Replace(" ", "") & "_InterfaceAssignments" & _InterfaceNumber & ".cfg"

        'see if we are changing or adding new settings
        Dim NewSettings As Boolean = True
        If File.Exists(File_SavePath) Then
            NewSettings = False
        End If

        'delete all settings found (should only be one) but this way it deletes all files from dir
        Dim MyPath As String = (_SavePath & "Interface" & _InterfaceNumber & "\" & InterfacePlugin._InterfaceName.Replace(" ", ""))
        If Directory.Exists(MyPath) Then
            'Delete all files from the Directory
            For Each filepath As String In Directory.GetFiles(MyPath)
                File.Delete(filepath)
            Next
        End If

        'Save New Settings
        Save_InterfaceAssignments(File_SavePath)

        'Load New Saved Settings to structure
        Dim objReader As New System.IO.StreamReader(File_SavePath)
        Dim SerializedString As String = objReader.ReadToEnd()
        objReader.Close()
        _InterfaceSettings = CType(DeSerializeInterfaceAssignments(SerializedString), InterfacePlugin.InterfaceAssignments) 'Structure Loaded Here

        'Check for Forced Restart before run
        If InterfacePlugin._Requires_Restart = True And _HasInitialized = False And NewSettings = True Then
            MsgBox("Settings Saved" & vbCrLf & "This Interface Requires GameEngine be Restarted" & vbCrLf & "Please Restart GameEngine", MsgBoxStyle.Information, "Interface Settings")
        Else
            MsgBox("Settings Saved", MsgBoxStyle.Information, "Interface Settings")
        End If
    End Sub

    'Load saved settings
    Public Sub LoadSavedSettings(ByVal FilePath As String)
        If System.IO.File.Exists(FilePath) = True Then
            Try
                Dim objReader As New System.IO.StreamReader(FilePath)
                Dim SerializedString As String = objReader.ReadToEnd()
                objReader.Close()
                _InterfaceSettings = CType(DeSerializeInterfaceAssignments(SerializedString), InterfacePlugin.InterfaceAssignments) 'Structure Loaded Here
                LoadFormFromStructure()
            Catch ex As Exception
                'clear the form
                ClearSettingsWindow()
                'load structure
                LoadStrutureFromForm() 'required - do not edit
                'delete bad file
                System.IO.File.Delete(FilePath)
            End Try
        End If

        'Reset the save button
        CheckSaveButton() 'required - do not edit
    End Sub

    'Load a preset file
    Public Sub Load_Preset(ProfileName As String)
        Dim FileName As String = _SavePath & "InterfacePresets\" & InterfacePlugin._InterfaceName.Replace(" ", "") & "_Presets\" & ProfileName & "_" & InterfacePlugin._InterfaceName.Replace(" ", "") & "_InterfaceAssignments.pre"
        If System.IO.File.Exists(FileName) = True Then
            Try
                Dim objReader As New System.IO.StreamReader(FileName)
                Dim SerializedString As String = objReader.ReadToEnd()
                objReader.Close()
                _InterfaceSettings = CType(DeSerializeInterfaceAssignments(SerializedString), InterfacePlugin.InterfaceAssignments) 'Structure Loaded Here
                LoadFormFromStructure()
                MsgBox(ProfileName & " Interface Preset Loaded!" & vbCrLf & "Remember to Click [Save] when finished.", MsgBoxStyle.Information, "Interface Preset")
            Catch ex As Exception
                'clear the form
                ClearSettingsWindow()
                'load structure
                LoadStrutureFromForm()
                'delete bad file
                System.IO.File.Delete(FileName)
                MsgBox(ProfileName & " Is a Invalid Interface Preset File!" & vbCrLf & "Bad or Corrupt file has been Deleted!", MsgBoxStyle.Critical, "Interface Preset")
            End Try
        End If

        'Reset the save button
        CheckSaveButton() 'required - do not edit
    End Sub

    Private Sub cb_StopMS_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_StopMS.SelectedIndexChanged

    End Sub

    Private Sub cb_StartMS_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_StartMS.SelectedIndexChanged

    End Sub

    Private Sub grb_NETOutput1_Enter(sender As Object, e As EventArgs) Handles grb_NETOutput1.Enter

    End Sub

    Private Sub Label230_Click(sender As Object, e As EventArgs) Handles Label230.Click

    End Sub

    Private Sub Label229_Click(sender As Object, e As EventArgs) Handles Label229.Click

    End Sub

    Private Sub Label215_Click(sender As Object, e As EventArgs) Handles Label215.Click

    End Sub

    Private Sub Label217_Click(sender As Object, e As EventArgs) Handles Label217.Click

    End Sub

    Private Sub Label218_Click(sender As Object, e As EventArgs) Handles Label218.Click

    End Sub

    Private Sub Label219_Click(sender As Object, e As EventArgs) Handles Label219.Click

    End Sub

    Private Sub txt_ShutDownOutput_TextChanged(sender As Object, e As EventArgs) Handles txt_ShutDownOutput.TextChanged

    End Sub

    Private Sub txt_StartUpOutput_TextChanged(sender As Object, e As EventArgs) Handles txt_StartUpOutput.TextChanged

    End Sub

    Private Sub Label220_Click(sender As Object, e As EventArgs) Handles Label220.Click

    End Sub

    Private Sub Label221_Click(sender As Object, e As EventArgs) Handles Label221.Click

    End Sub

    Private Sub Label222_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label223_Click(sender As Object, e As EventArgs) Handles Label223.Click

    End Sub

    Private Sub rad_HEX_CheckedChanged(sender As Object, e As EventArgs) Handles rad_HEX.CheckedChanged

    End Sub

    Private Sub rad_Decimal_CheckedChanged(sender As Object, e As EventArgs) Handles rad_Decimal.CheckedChanged

    End Sub

    Private Sub rad_Binary_CheckedChanged(sender As Object, e As EventArgs) Handles rad_Binary.CheckedChanged

    End Sub

    Private Sub Label224_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label225_Click(sender As Object, e As EventArgs)

    End Sub

#End Region
End Class
