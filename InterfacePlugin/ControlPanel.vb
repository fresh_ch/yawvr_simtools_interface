﻿Imports System.ComponentModel
Imports System.Net.Sockets
Public Class frmControlPanel

    Public myTCPclient As System.Net.Sockets.TcpClient
    Public myIPAddress As String

    Private Sub btn_Start_Click(sender As Object, e As EventArgs) Handles btn_Start.Click

        Try
            If myTCPclient.Connected = False Then
                ' No start/stop when the game plugin is not active.
                Exit Sub
            End If

            Dim networkStream As NetworkStream = myTCPclient.GetStream()
            If networkStream.CanWrite And networkStream.CanRead Then
                ' Do a simple write.
                Dim sendBytes As [Byte]() = {&HA1}
                networkStream.Write(sendBytes, 0, sendBytes.Length)
                ' Read the NetworkStream into a byte buffer.
                Dim bytes(myTCPclient.ReceiveBufferSize) As Byte
                networkStream.Read(bytes, 0, CInt(myTCPclient.ReceiveBufferSize))
            End If
            lbl_Status.Text = "Start sent."

        Catch ex As Exception
            lbl_Status.Text = "Error: " & ex.ToString()
            Exit Sub
        End Try
    End Sub

    Private Sub btn_Recenter_Click(sender As Object, e As EventArgs) Handles btn_Recenter.Click

        Try
            Dim TcpClient = New System.Net.Sockets.TcpClient()
            TcpClient.LingerState = New LingerOption(True, 0)
            TcpClient.Connect(myIPAddress, 50020)
            Dim networkStream As NetworkStream = TcpClient.GetStream()
            If networkStream.CanWrite And networkStream.CanRead Then
                ' Do a simple write.
                Dim sendBytes As [Byte]() = {&H55, &H0}
                networkStream.Write(sendBytes, 0, sendBytes.Length)
            End If
            TcpClient.Close()
            lbl_Status.Text = "Recenter sent."

        Catch ex As Exception
            lbl_Status.Text = "Error: " & ex.ToString()
            Exit Sub
        End Try

    End Sub

    Private Sub frmControlPanel_Load(sender As Object, e As EventArgs) Handles Me.Load

        lbl_Status.Text = ""

    End Sub

    Private Sub btn_Stop_Click(sender As Object, e As EventArgs) Handles btn_Stop.Click

        Try
            If myTCPclient.Connected = False Then
                ' No start/stop when the game plugin is not active.
                Exit Sub
            End If

            Dim networkStream As NetworkStream = myTCPclient.GetStream()
            If networkStream.CanWrite And networkStream.CanRead Then
                ' Do a simple write.
                Dim sendBytes As [Byte]() = {&HA2, &H0}
                networkStream.Write(sendBytes, 0, sendBytes.Length)
                ' Read the NetworkStream into a byte buffer.
                Dim bytes(myTCPclient.ReceiveBufferSize) As Byte
                networkStream.Read(bytes, 0, CInt(myTCPclient.ReceiveBufferSize))
            End If
            lbl_Status.Text = "Stop sent."

        Catch ex As Exception
            lbl_Status.Text = "Error: " & ex.ToString()
            Exit Sub
        End Try

    End Sub

    Private Sub frmControlPanel_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing

        Me.Hide()
        e.Cancel = True

    End Sub

    Private Sub trk_WashoutPower_Scroll(sender As Object, e As EventArgs) Handles trk_WashoutPower.Scroll
        YawVR.WashOutPower = trk_WashoutPower.Value
    End Sub

    Private Sub trk_Speed_Scroll(sender As Object, e As EventArgs) Handles trk_Speed.Scroll
        YawVR.Speed = trk_Speed.Value
    End Sub
End Class