﻿Option Strict On
Option Explicit On

Imports Interface_PluginAPI
Imports System.Net.Sockets
Imports System.Net
Imports System.Text
Imports System.Threading

Public Class InterfacePlugin
    Implements IPlugin_Interface
    Private _SavePath As String = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) & "\SimTools\GameEngine\"
    '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    '///        SimTools Interface Plugin - Edit the Settings below to provide support for your favorite Interface!                        ///
    '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    '////////////////////////////////////////////////////////////////////////////
    '///         Per Interface Settings - Change for Each Interface           ///
    '////////////////////////////////////////////////////////////////////////////
    Private Const _PluginAuthorsName As String = "MarcFdx" 'Authors Name Here
    Public Const _InterfaceName As String = "Yaw VR" 'Full Name of the Interface (Don't use the word 'Interface' in the name please)
    Public Const _Requires_Restart As Boolean = False 'Does this plugin require a restart with New / Removed settings
    '/////////////////////////////////////////////////////////////////////////////
    '///     What Interfaces SimTools will make this Plugin Available for      ///
    '/////////////////////////////////////////////////////////////////////////////
    Private Const _Enable_Interface1 As Boolean = True
    Private Const _Enable_Interface2 As Boolean = True
    Private Const _Enable_Interface3 As Boolean = True
    Private Const _Enable_Interface4 As Boolean = True
    Private Const _Enable_Interface5 As Boolean = True
    Private Const _Enable_Interface6 As Boolean = True
    '///////////////////////////////////////////////////////////////////////////////
    '///    The Interface Settings Structure - Edit the Elements as Needed       ///
    '///////////////////////////////////////////////////////////////////////////////
    Structure InterfaceAssignments
        Public _IPAddress1 As String
        Public _IPAddress2 As String
        Public _IPAddress3 As String
        Public _IPAddress4 As String
        Public _Port As String
        Public _OutputBits As String
        Public _OutputType As String
        Public _StartupOutput As String
        Public _HWStartMS As Integer
        Public _InterfaceOutput As String
        Public _ShutdownOutput As String
        Public _HWStopMS As Integer
        Public _OutPutRateMS As Integer '(REQUIRED - Do Not Change!) - this is the 'OutputRate' var that is returned to SimTools
        Public _WriteMmf As Integer
        Public _AutoOpenPanel As Integer
        Public _DeviceName As String
        Public _YawAxisMode As Integer
        Public _WashOut As Integer
        Public _Washoutpower As Integer
        Public _Speed As Integer

    End Structure

    '///////////////////////////////////////////////////////////////////////////////
    '///                     Edit these 6 Subroutines Below                      ///
    '///////////////////////////////////////////////////////////////////////////////
    'Used by GameEngine when the program starts up.
    Private Sub StartUp()
        'all startup commands here

        'Load the Output Char's - used to get the CHAR's for 0 thru 255 - makes output faster
        LoadAsciiCodes()
    End Sub

    'Used by GameEngine when the program is shutting down / switching plugins.
    Public Sub ShutDown() Implements Interface_PluginAPI.IPlugin_Interface.ShutDown
        'all shutdown / cleanup commands here  
    End Sub

    'Used by GameEngine (after startup above for all 6 interfaces) (all 6 interfaces can Initialize at the same time).
    Public Sub Initialize()
        'If Needed Initialize - Interface here (centering routine - mainly used for optical systems)       
    End Sub

    'Used by GameEngine when the Game Starts.    
    Public Function GameStart() As Boolean
        Try
            Dim IpAddress As String = MyForm._InterfaceSettings._IPAddress1 & "." & MyForm._InterfaceSettings._IPAddress2 & "." & MyForm._InterfaceSettings._IPAddress3 & "." & MyForm._InterfaceSettings._IPAddress4

            'create new UDPClient
            Interface_UdpClient = New UdpClient

            'Fix Output Strings - <ascii codes from number> - get ascii numbers
            StartupOutput = ReplaceWithAsciiCode(MyForm._InterfaceSettings._StartupOutput)
            InterfaceOutput = ReplaceWithAsciiCode(MyForm._InterfaceSettings._InterfaceOutput)
            ShutdownOutput = ReplaceWithAsciiCode(MyForm._InterfaceSettings._ShutdownOutput)

            StartYawConnection(MyForm, IpAddress)
            Threading.Thread.Sleep(MyForm._InterfaceSettings._HWStartMS)

            'Startup Udp Client
            Try
                Interface_UdpClient.Connect(IpAddress, CInt(MyForm._InterfaceSettings._Port))
            Catch ex As Exception
                'did not open           
                Return False
            End Try

            'Startup
            'Write Startup Output
            Dim bytCommand As Byte() = Text.Encoding.Default.GetBytes(StartupOutput)
            If StartupOutput <> "" Then
                Interface_UdpClient.Send(bytCommand, bytCommand.Length)
            End If
            Threading.Thread.Sleep(MyForm._InterfaceSettings._HWStartMS)

            'Set Output Types Needed to speed up GameSend
            OutputBits = MyForm._InterfaceSettings._OutputBits
            OutputType = MyForm._InterfaceSettings._OutputType

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    'Used by GameEngine to send values to the Interface.
    Public Sub Game_SendValues(Axis1a As Double, Axis2a As Double, Axis3a As Double, Axis4a As Double, Axis5a As Double, Axis6a As Double, Axis1b As Double, Axis2b As Double, Axis3b As Double, Axis4b As Double, Axis5b As Double, Axis6b As Double) Implements Interface_PluginAPI.IPlugin_Interface.Game_SendValues
        Dim Output As String
        Output = InterfaceOutput
        If Output.Contains("<Axis1a>") = True Then
            Dim FinalOut As String = GetOutPut(Axis1a, "1", OutputType)
            Output = Output.Replace("<Axis1a>", FinalOut)
        End If
        If Output.Contains("<Axis2a>") = True Then
            Dim FinalOut As String = GetOutPut(Axis2a, "2", OutputType)
            Output = Output.Replace("<Axis2a>", FinalOut)
        End If
        If Output.Contains("<Axis3a>") = True Then
            Dim FinalOut As String = GetOutPut(Axis3a, "3", OutputType)
            Output = Output.Replace("<Axis3a>", FinalOut)
        End If
        If Output.Contains("<Axis4a>") = True Then
            Dim FinalOut As String = GetOutPut(Axis4a, OutputBits, OutputType)
            Output = Output.Replace("<Axis4a>", FinalOut)
        End If
        If Output.Contains("<Axis5a>") = True Then
            Dim FinalOut As String = GetOutPut(Axis5a, OutputBits, OutputType)
            Output = Output.Replace("<Axis5a>", FinalOut)
        End If
        If Output.Contains("<Axis6a>") = True Then
            Dim FinalOut As String = GetOutPut(Axis6a, OutputBits, OutputType)
            Output = Output.Replace("<Axis6a>", FinalOut)
        End If
        If Output.Contains("<Axis1b>") = True Then
            Dim FinalOut As String = GetOutPut(Axis1b, OutputBits, OutputType)
            Output = Output.Replace("<Axis1b>", FinalOut)
        End If
        If Output.Contains("<Axis2b>") = True Then
            Dim FinalOut As String = GetOutPut(Axis2b, OutputBits, OutputType)
            Output = Output.Replace("<Axis2b>", FinalOut)
        End If
        If Output.Contains("<Axis3b>") = True Then
            Dim FinalOut As String = GetOutPut(Axis3b, OutputBits, OutputType)
            Output = Output.Replace("<Axis3b>", FinalOut)
        End If
        If Output.Contains("<Axis4b>") = True Then
            Dim FinalOut As String = GetOutPut(Axis4b, OutputBits, OutputType)
            Output = Output.Replace("<Axis4b>", FinalOut)
        End If
        If Output.Contains("<Axis5b>") = True Then
            Dim FinalOut As String = GetOutPut(Axis5b, OutputBits, OutputType)
            Output = Output.Replace("<Axis5b>", FinalOut)
        End If
        If Output.Contains("<Axis6b>") = True Then
            Dim FinalOut As String = GetOutPut(Axis6b, OutputBits, OutputType)
            Output = Output.Replace("<Axis6b>", FinalOut)
        End If

        'Write Interface Output
        bytCommand = Text.Encoding.Default.GetBytes(Output)
        Interface_UdpClient.Send(bytCommand, bytCommand.Length)
    End Sub

    'Used by GameEngine when the Game Stops.
    Public Sub GameStop()
        'Shutdown
        Threading.Thread.Sleep(MyForm._InterfaceSettings._HWStopMS)
        'Write Shutdown Output
        If ShutdownOutput <> "" Then
            bytCommand = Text.Encoding.Default.GetBytes(ShutdownOutput)
            Interface_UdpClient.Send(bytCommand, bytCommand.Length)
        End If
        Interface_UdpClient.Close()

        StopYawConnection(MyForm)

    End Sub

    '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    '///                                                 PLACE EXTRA NEEDED CODE/FUNCTIONS HERE                                                     ///
    '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#Region "Extra Code Needed for this interface"
    Private Interface_UdpClient As New UdpClient
    Private StartupOutput As String
    Private InterfaceOutput As String
    Private ShutdownOutput As String
    Private bytCommand As Byte()
    Private OutputBits As String = ""
    Private OutputType As String = ""

    'Chr ouput characters
    Private AsciiCodes As New ArrayList
    'load CHR codes for faster output

    Private Sub LoadAsciiCodes()
        AsciiCodes.Clear()
        For x = 0 To 255
            AsciiCodes.Add(Chr(x))
        Next
    End Sub

    'Returns the final output scaled to BitsNeeded and with Type
    Private Function GetOutPut(ByVal InputPercent As Double, ByVal BitsNeeded As String, ByVal Type As String) As String
        Dim OutPut As Double = 0
        Dim FinalOutput As String = ""

        'Bits
        Select Case BitsNeeded
            Case "1"
                ' YawVR setting: convert Percent to rotation degrees according to YawVR limitations
                ' and code on 12bit (4095)
                ' Yaw
                If MyForm.cbo_YawAxisMode.SelectedIndex = 1 Then
                    InputPercent = ConvertYawForceToRotation(InputPercent)
                ElseIf MyForm.cbo_YawAxisMode.SelectedIndex = 2 Then
                    InputPercent = ConvertYawHeadingToYawForce(InputPercent)
                    'MyForm._ControlPanel.lbl_Config1.Text = "1_H2F" & Format(InputPercent, "0.00")
                    InputPercent = ConvertYawForceToRotation(InputPercent)
                    'MyForm._ControlPanel.lbl_Config2.Text = "2_F2R" & Format(InputPercent, "0.00")
                End If

                If InputPercent > 0 Then
                    OutPut = (InputPercent * 2048) + 2047
                ElseIf InputPercent < 0 Then
                    OutPut = 2047 - (InputPercent * -2047)
                Else
                    OutPut = 2047
                End If
                'If InputPercent > 0 Then
                '    OutPut = (InputPercent * 2048 * YawCoeff) + 2047
                'ElseIf InputPercent < 0 Then
                '    OutPut = 2047 - (InputPercent * -2047 * YawCoeff)
                'Else
                '    OutPut = 2047
                'End If
            Case "2"
                ' YawVR setting: convert Percent to rotation degrees according to YawVR limitations
                ' and code on 12bit (4095)
                ' Pitch
                If InputPercent > 0 Then
                    OutPut = (InputPercent * 2048 * PitchFCoeff) + 2047
                ElseIf InputPercent < 0 Then
                    OutPut = 2047 - (InputPercent * -2047 * PitchBCoeff)
                Else
                    OutPut = 2047
                End If
            Case "3"
                ' YawVR setting: convert Percent to rotation degrees according to YawVR limitations
                ' and code on 12bit (4095)
                ' Roll
                If InputPercent > 0 Then
                    OutPut = (InputPercent * 2048 * RollCoeff) + 2047
                ElseIf InputPercent < 0 Then
                    OutPut = 2047 - (InputPercent * -2047 * RollCoeff)
                Else
                    OutPut = 2047
                End If
        End Select

        'Convert to an integer
        OutPut = Math.Round(OutPut, 0)

        'Type
        Select Case Type
            Case "Binary"
                FinalOutput = GetChr(OutPut)
                'output same number of char's always
                If CInt(BitsNeeded) > 8 Then
                    If FinalOutput.Length = 1 Then
                        FinalOutput = CStr(AsciiCodes(0)) & FinalOutput
                    End If
                End If
                If CInt(BitsNeeded) > 16 Then
                    If FinalOutput.Length = 2 Then
                        FinalOutput = CStr(AsciiCodes(0)) & FinalOutput
                    End If
                End If
                If CInt(BitsNeeded) > 24 Then
                    If FinalOutput.Length = 3 Then
                        FinalOutput = CStr(AsciiCodes(0)) & FinalOutput
                    End If
                End If
            Case "Hex"
                FinalOutput = GetHex(OutPut)
            Case "Decimal"
                FinalOutput = CStr(OutPut)
        End Select
        '_JRK_Out:  'special output jump for JRK output
        Return FinalOutput
    End Function

    'Returns output HEX value
    Private Function GetHex(ByVal HexCode As Double) As String
        Return CStr(Conversion.Hex(HexCode))
    End Function

    'Returns output Chr value
    Private Function GetChr(ByVal CharCode As Double) As String
        Dim ChrStringOut As String = ""
        If CharCode > 16777215 Then
            Dim Multiplierx1 As Double = CharCode / 16777216
            If Multiplierx1 > 255 Then
                Multiplierx1 = 255
            End If
            Dim IntegralPart As Double = Math.Truncate(Multiplierx1)
            Try
                CharCode = CharCode - (IntegralPart * 16777216)
            Catch ex As Exception
                CharCode = 0
            End Try
            ChrStringOut = (AsciiCodes(CInt(IntegralPart))).ToString
        End If
        If CharCode > 65535 Then
            Dim Multiplierx1 As Double = CharCode / 65536
            If Multiplierx1 > 255 Then
                Multiplierx1 = 255
            End If
            Dim IntegralPart As Double = Math.Truncate(Multiplierx1)
            Try
                CharCode = CharCode - (CDbl(IntegralPart) * 65536)
            Catch ex As Exception
                CharCode = 0
            End Try
            ChrStringOut = ChrStringOut & (AsciiCodes(CInt(IntegralPart))).ToString
        End If
        If CharCode > 255 Then
            Dim Multiplierx1 As Double = CharCode / 256.0
            If Multiplierx1 > 255 Then
                Multiplierx1 = 255
            End If
            Dim IntegralPart As Double = Math.Truncate(Multiplierx1)
            Try
                CharCode = CharCode - (CDbl(IntegralPart) * 256)
            Catch ex As Exception
                CharCode = 0
            End Try
            ChrStringOut = ChrStringOut & (AsciiCodes(CInt(IntegralPart))).ToString
        End If
        ChrStringOut = ChrStringOut & (AsciiCodes(CInt(CharCode))).ToString
        Return ChrStringOut
    End Function

    'Replaces output strings in a < # > with a chr - example <63> = ?
    Public Function ReplaceWithAsciiCode(ByVal InputString As String) As String
        If InputString = "-" Or InputString = "" Then
            Return InputString
        End If
        For x = 0 To 255
            Dim SearchString As String = ("<" & x.ToString & ">").ToString
            If InputString.Contains(SearchString) Then
                Dim ReplaceString As String = (Chr(x)).ToString
                InputString = InputString.Replace(SearchString, ReplaceString)
            End If
        Next
        Return InputString
    End Function
#End Region

    '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    '///                                                        DO NOT EDIT BELOW HERE!!!                                                           ///
    '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#Region "SimTools Required Functions - Do Not Change"
    Public MyForm As New UserControl_Settings() 'Our Settings Form for this interface
    Private GameStarted_ProperSettings As Boolean = False 'Only start if there is proper settings

    'initilize startup
    Public Sub MyInitialize() Implements Interface_PluginAPI.IPlugin_Interface.Initialize
        'run the Initialize routine above
        Initialize()

        'Set the HasInitialized flag to True
        MyForm._HasInitialized = True
    End Sub

    'game start
    Public Function Game_Start() As Boolean Implements Interface_PluginAPI.IPlugin_Interface.GameStart
        'Only startup if there is a settings file AND we are not already running.
        Dim FinalPath As String = _SavePath & "Interface" & MyForm._InterfaceNumber & "\" & InterfacePlugin._InterfaceName.Replace(" ", "") & "\" & InterfacePlugin._InterfaceName.Replace(" ", "") & "_InterfaceAssignments" & MyForm._InterfaceNumber & ".cfg"
        If System.IO.File.Exists(FinalPath) = True And GameStarted_ProperSettings = False Then
            GameStarted_ProperSettings = True
            Return GameStart()
        End If
        'could not start
        Return False
    End Function

    'game end
    Public Sub Game_End() Implements Interface_PluginAPI.IPlugin_Interface.GameEnd
        'Stop Engine if it has been started
        If GameStarted_ProperSettings = True Then
            GameStarted_ProperSettings = False
            GameStop()
            'pause for external app/Interface to recieve the data
            System.Threading.Thread.Sleep(100)
        End If
    End Sub

    'set what interface number
    Public WriteOnly Property InterfaceNumber As Integer Implements Interface_PluginAPI.IPlugin_Interface.WhatInterface
        Set(value As Integer)
            MyForm._InterfaceNumber = value
        End Set
    End Property

    'gets the window and loads settings if found
    Public Function GetSettingsWindow() As Object Implements Interface_PluginAPI.IPlugin_Interface.GetSettingsWindow
        'current path
        Dim FinalPath As String = _SavePath & "Interface" & MyForm._InterfaceNumber & "\" & InterfacePlugin._InterfaceName.Replace(" ", "") & "\" & InterfacePlugin._InterfaceName.Replace(" ", "") & "_InterfaceAssignments" & MyForm._InterfaceNumber & ".cfg"
        'Run Startup Commands Above
        StartUp()
        'see if there are any saved settings
        If IO.File.Exists(FinalPath) Then
            MyForm.LoadSavedSettings(FinalPath)
        Else
            ResetSettingsWindow()
        End If
        Return MyForm 'return groupbox
    End Function

    'Clear_AxisAssignments form
    Public Sub ResetSettingsWindow() Implements Interface_PluginAPI.IPlugin_Interface.ResetSettingsWindow
        'clear the form
        MyForm.ClearSettingsWindow()
        'load structure
        MyForm.LoadStrutureFromForm() 'required - do not edit
        'Reset the save button
        MyForm.CheckSaveButton() 'required - do not edit
    End Sub

    'Save a preset
    Public Sub SavePreset(Name As String) Implements Interface_PluginAPI.IPlugin_Interface.SavePreset
        MyForm.SaveThisPreset(Name)
    End Sub

    'Load a preset
    Public Sub LoadPreset(ProName As String) Implements Interface_PluginAPI.IPlugin_Interface.LoadPreset
        MyForm.Load_Preset(ProName)
    End Sub

    'what axis assignments enable for
    Public ReadOnly Property Enable_Interface1 As Boolean Implements Interface_PluginAPI.IPlugin_Interface.Enable_Interface1
        Get
            Return _Enable_Interface1
        End Get
    End Property

    Public ReadOnly Property Enable_Interface2 As Boolean Implements Interface_PluginAPI.IPlugin_Interface.Enable_Interface2
        Get
            Return _Enable_Interface2
        End Get
    End Property

    Public ReadOnly Property Enable_Interface3 As Boolean Implements Interface_PluginAPI.IPlugin_Interface.Enable_Interface3
        Get
            Return _Enable_Interface3
        End Get
    End Property

    Public ReadOnly Property Enable_Interface4 As Boolean Implements Interface_PluginAPI.IPlugin_Interface.Enable_Interface4
        Get
            Return _Enable_Interface4
        End Get
    End Property

    Public ReadOnly Property Enable_Interface5 As Boolean Implements Interface_PluginAPI.IPlugin_Interface.Enable_Interface5
        Get
            Return _Enable_Interface5
        End Get
    End Property

    Public ReadOnly Property Enable_Interface6 As Boolean Implements Interface_PluginAPI.IPlugin_Interface.Enable_Interface6
        Get
            Return _Enable_Interface6
        End Get
    End Property

    'get name
    Public ReadOnly Property InterfaceName As String Implements Interface_PluginAPI.IPlugin_Interface.InterfaceName
        Get
            Return _InterfaceName
        End Get
    End Property

    'get author
    Public ReadOnly Property PluginAuthorsName As String Implements Interface_PluginAPI.IPlugin_Interface.PluginAuthorsName
        Get
            Return _PluginAuthorsName
        End Get
    End Property

    'requires restart?
    Public ReadOnly Property Requires_Restart As Boolean Implements Interface_PluginAPI.IPlugin_Interface.Requires_Restart
        Get
            Return _Requires_Restart
        End Get
    End Property

    'Has Initialized?
    Public ReadOnly Property Has_Initialized As Boolean Implements Interface_PluginAPI.IPlugin_Interface.Has_Initialized
        Get
            Return MyForm._HasInitialized
        End Get
    End Property

    'output rate
    Public ReadOnly Property OutPutRate As Integer Implements Interface_PluginAPI.IPlugin_Interface.OutputRate
        Get
            Return MyForm._InterfaceSettings._OutPutRateMS
        End Get
    End Property
#End Region
End Class




