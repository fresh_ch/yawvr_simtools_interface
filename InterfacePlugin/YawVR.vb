﻿Option Strict On
Option Explicit On

Imports System.Net.Sockets
Imports System.IO
Imports System.IO.MemoryMappedFiles
Imports System.Net
Imports System.Text
Imports System.Threading

Module YawVR

    Public Yaw, Pitch, Roll, Batt As Single
    Public DofMode As Single = 1.4
    Public ConfigPower, ConfigYaw, ConfigRoll, ConfigPitchF, ConfigPitchB, ConfigYawLimit As Single
    Public YawCoeff, PitchBCoeff, PitchFCoeff, RollCoeff As Single
    Public WashOutPower As Integer
    Public Speed As Integer

    Private tcpClient As System.Net.Sockets.TcpClient
    Private UDPCLient As UdpClient
    Private ControlPanel As New frmControlPanel()
    Private ThreadRunning As Boolean
    Private WithMmf As Boolean
    Private ActiveDevice As String
    Private YawAxisMode As Integer
    Private WashOut As Integer
    Private NewWashout, WashoutRunning As Boolean

    Public Structure YawDevice
        Public IpAddress As String
        Public Name As String
        Public DisplayName As String
        Public MsgString As String
    End Structure
    Public YawDevices() As YawDevice

    Public Sub StartYawConnection(MyForm As UserControl_Settings, IpAddress As String)

        Try
            ' ControlPanel must be instanciated by the UserControl_Settings object, otherwise the user.forms are not available
            ControlPanel = MyForm._ControlPanel
            YawAxisMode = MyForm._InterfaceSettings._YawAxisMode
            WashOut = MyForm._InterfaceSettings._WashOut
            WashOutPower = MyForm._InterfaceSettings._Washoutpower
            Speed = MyForm._InterfaceSettings._Speed

            tcpClient = New System.Net.Sockets.TcpClient()
            tcpClient.LingerState = New LingerOption(True, 0)
            PushTcpClientToConfigPanel()
            tcpClient.Connect(IpAddress, 50020)

            Dim networkStream As NetworkStream = tcpClient.GetStream()
            If networkStream.CanWrite And networkStream.CanRead Then
                ' Do a simple write.
                ' byte0 H30, byte 1-4 UDP Port, byte 5+ Client name (SimTools)
                Dim sendBytes As [Byte]() = {&H30, &H0, &H0, &H71, &H8B, &H53, &H69, &H6D, &H54, &H6F, &H6F, &H6C, &H73}
                networkStream.Write(sendBytes, 0, sendBytes.Length)
                ' Read the NetworkStream into a byte buffer.
                Dim bytes(tcpClient.ReceiveBufferSize) As Byte
                networkStream.Read(bytes, 0, CInt(tcpClient.ReceiveBufferSize))
                ' Output the data received from the host to the console.
                Dim returndata As String = Encoding.ASCII.GetString(bytes)
                System.Threading.Thread.Sleep(100)
                ' Get Configuration from YawVR and calculates the max travel set in the config app.
                If Not MyForm.cbo_Devices.SelectedItem.ToString.Contains("EMULATOR") Then
                    sendBytes = {&HF6} ' GetConfig command
                    networkStream.Write(sendBytes, 0, sendBytes.Length)
                    ' Read the NetworkStream into a byte buffer.
                    networkStream.Read(bytes, 0, CInt(tcpClient.ReceiveBufferSize))
                    ConfigPower = bytes(4)
                    ConfigPitchB = bytes(9)
                    ConfigPitchF = bytes(13)
                    ConfigRoll = bytes(17)
                    ConfigYaw = bytes(22)
                    ConfigYawLimit = bytes(24)
                Else
                    ' The emulator doesn't respond to the F6 command. Using default values.
                    ConfigPower = 50
                    ConfigPitchB = 15
                    ConfigPitchF = 10
                    ConfigRoll = 15
                    ConfigYaw = 90
                    ConfigYawLimit = 1
                End If
                If ConfigYawLimit = 1 Then
                    ' Yaw is limited, we must calculate the coefficient
                    YawCoeff = ConfigYaw / 180
                Else
                    YawCoeff = 1
                    End If
                    If ConfigPitchB > 0 Then
                        PitchBCoeff = ConfigPitchB / 180
                    Else
                        PitchBCoeff = 0.1
                    End If
                    If ConfigPitchF > 0 Then
                        PitchFCoeff = ConfigPitchF / 180
                    Else
                        PitchFCoeff = 0.1
                    End If
                    If ConfigRoll > 0 Then
                        RollCoeff = ConfigRoll / 180
                    Else
                        RollCoeff = 0.1
                    End If

                    ' Config Panel
                    DisplayConfig(True)

                    ' Write MMF for motion compensation if configured
                    If MyForm._InterfaceSettings._WriteMmf = 1 Then
                        WithMmf = True
                    Else
                        WithMmf = False
                    End If

                    Dim thread As New Thread(AddressOf TransferYawData_Thread)
                    thread.Priority = ThreadPriority.AboveNormal
                    thread.Start()
                Else
                    If Not networkStream.CanRead Then
                    'textStatus.Text = "cannot not write data to this stream"
                    tcpClient.Close()
                Else
                    If Not networkStream.CanWrite Then
                        'textStatus.Text = "cannot read data from this stream"
                        tcpClient.Close()
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox("Fatal error! YawVR Interface not started." & vbCrLf & ex.ToString())
            'textStatus.Text = "Error: " & ex.ToString()
            Exit Sub
        End Try

    End Sub

    Public Sub StopYawConnection(MyForm As UserControl_Settings)

        Try
            'Dim tcpClient As New System.Net.Sockets.TcpClient()
            'tcpClient.Connect(textIPAddress.Text, 50020)

            Dim networkStream As NetworkStream = tcpClient.GetStream()

            If networkStream.CanWrite And networkStream.CanRead Then
                ' byte0 HA3 = magic byte to stop connection
                Dim sendBytes As [Byte]() = {&HA3}
                networkStream.Write(sendBytes, 0, sendBytes.Length)
                ' Read the NetworkStream into a byte buffer.
                Dim bytes(tcpClient.ReceiveBufferSize) As Byte
                networkStream.Read(bytes, 0, CInt(tcpClient.ReceiveBufferSize))
            Else
                If Not networkStream.CanRead Then
                    'ListeningStatus = "cannot not write data to this stream"
                    tcpClient.Close()
                Else
                    If Not networkStream.CanWrite Then
                        'ListeningStatus = "cannot read data from this stream"
                        tcpClient.Close()
                    End If
                End If
            End If
            ' close to provoke RST packet
            tcpClient.LingerState = New LingerOption(True, 0)
            tcpClient.Client.Close()
            tcpClient.Close()


            ' Finally stop the listener and close the UDP port.
            ThreadRunning = False
            System.Threading.Thread.Sleep(100)
            DisplayConfig(False)

        Catch ex As Exception

        End Try

    End Sub

    Private Function TransferYawData_Thread() As Boolean

        Dim UDPClient As UdpClient = New UdpClient()

        ThreadRunning = True
        UDPClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, True)
        UDPClient.Client.Bind(New IPEndPoint(IPAddress.Any, 29067))

        Try
            Dim iepRemoteEndPoint As IPEndPoint = New IPEndPoint(IPAddress.Any, 28067)
            Dim strMessage As String = String.Empty

            Do
                Dim bytRecieved As Byte() = UDPClient.Receive(iepRemoteEndPoint)
                strMessage = Encoding.ASCII.GetString(bytRecieved)
                Yaw = GetValueFromString(strMessage, "SY[")
                Pitch = GetValueFromString(strMessage, "SP[")
                Roll = GetValueFromString(strMessage, "SR[")
                Batt = GetValueFromString(strMessage, "U[")
                WriteMMF(False)
                System.Threading.Thread.Sleep(5)
            Loop While (ThreadRunning = True)

            WriteMMF(True)

            UDPClient.Close()
            Return True

        Catch e2 As Exception

            ThreadRunning = False
            Return False

        End Try

    End Function

    Private Sub DisplayConfig(connecting As Boolean)

        Try
            Dim frmCollection = System.Windows.Forms.Application.OpenForms
            If frmCollection.OfType(Of frmControlPanel).Any Then

                If connecting = False Then
                    ControlPanel.lbl_title.Text = "Active device:"
                    ControlPanel.lbl_Config1.Text = ActiveDevice
                    ControlPanel.lbl_Config2.Text = "Interface stopped."
                    ControlPanel.btn_Start.Enabled = False
                    ControlPanel.btn_Stop.Enabled = False
                    ControlPanel.btn_Recenter.Enabled = True
                    ControlPanel.myTCPclient = tcpClient
                    'ControlPanel.Dispose()

                Else
                    ActiveDevice = ControlPanel.lbl_Config1.Text
                    ControlPanel.myTCPclient = tcpClient
                    Dim strText As String = ""
                    If ConfigYawLimit = 0 Then
                        strText = "Yaw: no limit"
                    Else
                        strText = "Yaw: " & ConfigYaw & "°"
                    End If
                    strText = strText & "     Roll: " & ConfigRoll & "°"
                    ControlPanel.lbl_title.Text = "Device limits:"
                    ControlPanel.lbl_Config1.Text = strText
                    strText = "Pitch back: " & ConfigPitchB & "°    Pitch front:" & ConfigPitchF & "°"
                    ControlPanel.lbl_Config2.Text = strText
                    ControlPanel.btn_Start.Enabled = True
                    ControlPanel.btn_Stop.Enabled = True
                    ControlPanel.btn_Recenter.Enabled = False
                    ControlPanel.myTCPclient = tcpClient

                End If

            End If

        Catch ex As Exception

        End Try


    End Sub

    Private Sub PushTcpClientToConfigPanel()

        ' Control panel must have access to the tcpClient object
        If ControlPanel.IsDisposed = False Then
            ControlPanel.myTCPclient = tcpClient
        End If

    End Sub
    Private Sub WriteMMF(ResetPosition As Boolean)

        If WithMmf = False Then
            Exit Sub
        End If

        Dim mmf As MemoryMappedFile

        Try
            mmf = MemoryMappedFile.OpenExisting("YawVRGEFile")
        Catch ex As Exception
            mmf = MemoryMappedFile.CreateNew("YawVRGEFile", 32)
        End Try

        Dim stream As MemoryMappedViewStream = mmf.CreateViewStream()
        Dim writer As BinaryWriter = New BinaryWriter(stream)

        If ResetPosition = True Then
            writer.Write(BitConverter.GetBytes(0))
            writer.Write(BitConverter.GetBytes(0))
            writer.Write(BitConverter.GetBytes(0))
        Else
            writer.Write(BitConverter.GetBytes(Yaw))
            writer.Write(BitConverter.GetBytes(Pitch))
            writer.Write(BitConverter.GetBytes(Roll))
        End If
        writer.Write(BitConverter.GetBytes(Batt))
        writer.Write(BitConverter.GetBytes(DofMode))
        writer.Flush()
        'MsgBox("Pitch: " & Pitch.ToString, MsgBoxStyle.ApplicationModal, "Information")

    End Sub
    Private Function GetValueFromString(toParse As String, toFind As String) As Single

        Dim subString As String = ""
        Dim foundPlace As Integer
        foundPlace = InStr(toParse, toFind)
        If foundPlace > 0 Then
            subString = toParse.Substring(foundPlace + toFind.Length - 1)
            foundPlace = InStr(subString, "]")
            subString = subString.Substring(0, foundPlace - 1)
        End If

        GetValueFromString = Convert.ToSingle(subString)

    End Function

    Public Sub FindDevices()

        UDPCLient = New UdpClient(50552)
        Dim ip As New IPEndPoint(IPAddress.Broadcast, 50010)
        Dim bytes As Byte() = Encoding.ASCII.GetBytes("YAW_CALLING")
        UDPCLient.Send(bytes, bytes.Length, ip)
        UDPCLient.Close()

        UDPCLient = New UdpClient()
        UDPCLient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, True)
        UDPCLient.Client.Bind(New IPEndPoint(IPAddress.Any, 50552))

        Try
            Dim iepRemoteEndPoint As IPEndPoint = New _
            IPEndPoint(IPAddress.Any, 0)
            Dim strMessage As String = String.Empty

            ReDim YawDevices(0)
            UDPCLient.BeginReceive(AddressOf Receive, New Object())

            Dim intCount As Integer = 0
            Do
                System.Threading.Thread.Sleep(100)
                intCount = intCount + 1
            Loop Until intCount = 10

            UDPCLient.Close()

        Catch e2 As Exception
            MsgBox("Fatal error: could not search for YawVR devices.", vbExclamation)
        End Try

    End Sub

    Private Sub UDPHelloListner()
        UDPCLient.BeginReceive(AddressOf Receive, New Object())
    End Sub
    Private Sub Receive(ByVal ar As IAsyncResult)

        Try
            Dim ip As New IPEndPoint(IPAddress.Any, 50010)
            Dim bytes As Byte() = UdpClient.EndReceive(ar, ip)
            Dim message As String = Encoding.ASCII.GetString(bytes)
            Dim NbYaw As Integer
            Dim DevString() As String

            If message.StartsWith("YAWDEVICE") Then
                If IsNothing(YawDevices) Then
                    NbYaw = 0
                Else
                    NbYaw = YawDevices.Length
                    For Each dev In YawDevices
                        If dev.MsgString = message Then
                            Exit Sub
                        End If
                    Next
                    Array.Resize(YawDevices, NbYaw + 1)
                    DevString = Split(message, ";")
                    YawDevices(NbYaw).MsgString = message
                    YawDevices(NbYaw).Name = DevString(2)
                    YawDevices(NbYaw).IpAddress = ip.Address.ToString
                    YawDevices(NbYaw).DisplayName = DevString(2) & " (" & ip.Address.ToString & ")"
                End If
            End If
            UDPHelloListner()

        Catch ex As Exception
            ' An exception is needed to stop the Receive event.
        End Try

    End Sub

    Public Function ConvertYawForceToRotation(YawForce As Double) As Double

        ' Warning: NextYawValue is in degrees, YawForce is in percent
        Dim NextYawValue As Double
        Dim SpeedCoeff As Double = 30
        Dim Deadzone As Double = 0.05

        If WashOut = 1 And ConfigYawLimit = 1 And YawForce <= Deadzone And YawForce >= -Deadzone Then
            YawForce = 0
        End If

        If YawForce <> 0 Then
            NextYawValue = Yaw + (YawForce * SpeedCoeff)
            NewWashout = True
            If ConfigYawLimit = 1 And (NextYawValue > ConfigYaw Or NextYawValue < -ConfigYaw) Then
                ' Respect config limits
                If NextYawValue > 0 Then
                    NextYawValue = ConfigYaw
                Else
                    NextYawValue = -ConfigYaw
                End If
            End If
        Else
            ' No movement from game: washout if axis is limited and washout enabled
            If ConfigYawLimit = 1 And WashOut = 1 Then
                NextYawValue = ApplyWashout()
            Else
                NextYawValue = Yaw
            End If
        End If

            'ControlPanel.lbl_Status.Text = "Seat Yaw=" & Yaw.ToString & " Next Yaw=" & NextYawValue.ToString & " Washing=" & Washout.ToString & " YawForce=" & YawForce.ToString
            ConvertYawForceToRotation = NextYawValue / 180

    End Function

    Public Function ConvertYawHeadingToYawForce(YawHeading As Double) As Double
        Static PreviousYawHeading(10) As Double
        Dim YawDelta, YawDeltaWithSpeed As Double
        Dim SpeedCoeff As Double

        ' Update previous values
        Dim cnt As Integer
        For cnt = 1 To 9
            PreviousYawHeading(cnt) = PreviousYawHeading(cnt + 1)
        Next
        PreviousYawHeading(10) = YawHeading

        SpeedCoeff = Convert.ToDouble(Speed)
        YawDelta = (PreviousYawHeading(1) - YawHeading) * 180
        If Speed > 0 Then
            YawDeltaWithSpeed = YawDelta * Speed
        ElseIf Speed < 0 Then
            YawDeltaWithSpeed = YawDelta / -Speed
        Else
            YawDeltaWithSpeed = YawDelta
        End If
        'ControlPanel.lbl_Status.Text = "Yaw=" & Format(YawHeading * 180, "###.00") & "Yaw Delta=" & Format(YawDelta, "###.00") & " Yaw Force=" & Format(YawDeltaWithSpeed, "###.00")

        If Math.Abs(YawDeltaWithSpeed) < 70 Then ' Eliminate too large deltas
            ConvertYawHeadingToYawForce = YawDeltaWithSpeed / 20
            If ConvertYawHeadingToYawForce > 1 Then
                ConvertYawHeadingToYawForce = 1
            ElseIf ConvertYawHeadingToYawForce < -1 Then
                ConvertYawHeadingToYawForce = -1
            End If
        Else
            ' Eliminate sudden heading changes (for example start from zero ou go to zero, or past 360)
            ConvertYawHeadingToYawForce = 0
        End If

    End Function

    Private Function ApplyWashout() As Double

        Static WashoutCounter As Long
        Static PreviousYaw(100) As Double
        Dim WashoutDelta As Double
        Dim WashoutCoeff As Double
        Dim WashoutAccel As Double
        Dim WashoutDecel As Double
        Dim WashoutMaxPower As Double = 10 + WashOutPower

        ' Washout start
        If NewWashout = True Then
            WashoutCounter = 0
            If Math.Abs(Yaw) > 15 Then
                NewWashout = False
                WashoutRunning = True
            Else
                'ControlPanel.lbl_Status.Text = "No washout needed"
                NewWashout = True
                WashoutRunning = False
                ApplyWashout = Yaw
                Exit Function
            End If
        End If

        ' Washout end
        If Math.Abs(Yaw) <= 5 Then
            'ControlPanel.lbl_Status.Text = "End of washout"
            WashoutCounter = 0
            NewWashout = True
            WashoutRunning = False
            ApplyWashout = Yaw
            Exit Function
        End If

        If WashoutRunning = False Then
            NewWashout = True
            Exit Function
        End If

        ' Do not move before 1 sec.
        If WashoutCounter < 101 Then
            WashoutCounter += 1
            ApplyWashout = Yaw
            Exit Function
        End If

        ' Update PreviousYaw
        Dim cnt As Integer
        For cnt = 1 To 49
            PreviousYaw(cnt) = PreviousYaw(cnt + 1)
        Next
        PreviousYaw(50) = Yaw

        ' Calculate delta to adapt speed
        WashoutDelta = Math.Abs(PreviousYaw(40) - Yaw)
        'ControlPanel.lbl_Config1.Text = "Counter=" & Format(WashoutCounter, "#") & " Wash. coeff=" & Format(WashoutCoeff, "00.0") & " Delta=" & Format(WashoutDelta, "00.0") & "PrevYaw=" & Format(PreviousYaw(1), "00.0") & "Yaw=" & Format(Yaw, "00.0")
        If WashoutDelta < 0 Then
            WashoutDelta = 0
        End If
        If WashoutDelta > 6 Then
            WashoutDelta = 6
        End If

        ' At beginning, apply a speed reduction 
        WashoutAccel = WashoutCounter / 100 * 3
        If WashoutAccel > WashoutMaxPower Then
            WashoutAccel = WashoutMaxPower
        End If
        WashoutAccel = WashoutMaxPower - WashoutAccel

        ' When arriving at center, reduce speed
        If Math.Abs(Yaw) < 10 Then
            WashoutDecel = (10 - Math.Abs(Yaw)) / 2
        Else
            WashoutDecel = 0
        End If

        ' Main formula
        WashoutCoeff = WashoutMaxPower - WashoutDelta - WashoutAccel - WashoutDecel
        'ControlPanel.lbl_Status.Text = "Counter=" & Format(WashoutCounter, "#") & " Wash. coeff=" & Format(WashoutCoeff, "00.0") & " Accel=" & Format(WashoutAccel, "00.0") & " Decel=" & Format(WashoutDecel, "00.0")


        ' Security: speed limit
        If WashoutCoeff > WashoutMaxPower Then
            WashoutCoeff = WashoutMaxPower
        End If
        ' Security: speed limit
        If WashoutCoeff < 0 Then
            WashoutCoeff = 0
        End If

        ApplyWashout = Yaw
        If Yaw > 0 Then
            ApplyWashout = Yaw - WashoutCoeff
            If Yaw < 0 Then
                Yaw = 0
            End If
        ElseIf Yaw < 0 Then
            ApplyWashout = Yaw + WashoutCoeff
            If Yaw > 0 Then
                Yaw = 0
            End If
        End If
        WashoutCounter += 1

    End Function
End Module
