﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class UserControl_Settings
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UserControl_Settings))
        Me.grb_NETOutput1 = New System.Windows.Forms.GroupBox()
        Me.Label216 = New System.Windows.Forms.Label()
        Me.trk_WashoutPower = New System.Windows.Forms.TrackBar()
        Me.trk_Speed = New System.Windows.Forms.TrackBar()
        Me.Chk_Washout = New System.Windows.Forms.CheckBox()
        Me.cbo_YawAxisMode = New System.Windows.Forms.ComboBox()
        Me.btn_Rescan = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbo_Devices = New System.Windows.Forms.ComboBox()
        Me.chk_AutoOpenPanel = New System.Windows.Forms.CheckBox()
        Me.btn_OpenPanel = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.chk_WriteMmf = New System.Windows.Forms.CheckBox()
        Me.cb_StopMS = New System.Windows.Forms.ComboBox()
        Me.cb_StartMS = New System.Windows.Forms.ComboBox()
        Me.cb_PacketRate = New System.Windows.Forms.ComboBox()
        Me.btn_Save = New System.Windows.Forms.Button()
        Me.Label230 = New System.Windows.Forms.Label()
        Me.Label229 = New System.Windows.Forms.Label()
        Me.txt_Port = New System.Windows.Forms.TextBox()
        Me.txt_IPAddress4 = New System.Windows.Forms.TextBox()
        Me.txt_IPAddress3 = New System.Windows.Forms.TextBox()
        Me.txt_IPAddress2 = New System.Windows.Forms.TextBox()
        Me.txt_IPAddress1 = New System.Windows.Forms.TextBox()
        Me.Label215 = New System.Windows.Forms.Label()
        Me.Label217 = New System.Windows.Forms.Label()
        Me.Label218 = New System.Windows.Forms.Label()
        Me.Label219 = New System.Windows.Forms.Label()
        Me.txt_InterfaceOutput = New System.Windows.Forms.TextBox()
        Me.txt_ShutDownOutput = New System.Windows.Forms.TextBox()
        Me.txt_StartUpOutput = New System.Windows.Forms.TextBox()
        Me.Label220 = New System.Windows.Forms.Label()
        Me.Label221 = New System.Windows.Forms.Label()
        Me.Label223 = New System.Windows.Forms.Label()
        Me.rad_HEX = New System.Windows.Forms.RadioButton()
        Me.rad_Decimal = New System.Windows.Forms.RadioButton()
        Me.rad_Binary = New System.Windows.Forms.RadioButton()
        Me.cb_OutputBits = New System.Windows.Forms.ComboBox()
        Me.grb_NETOutput1.SuspendLayout()
        CType(Me.trk_WashoutPower, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trk_Speed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grb_NETOutput1
        '
        Me.grb_NETOutput1.BackColor = System.Drawing.Color.Transparent
        Me.grb_NETOutput1.BackgroundImage = CType(resources.GetObject("grb_NETOutput1.BackgroundImage"), System.Drawing.Image)
        Me.grb_NETOutput1.Controls.Add(Me.btn_Save)
        Me.grb_NETOutput1.Controls.Add(Me.Label216)
        Me.grb_NETOutput1.Controls.Add(Me.trk_WashoutPower)
        Me.grb_NETOutput1.Controls.Add(Me.trk_Speed)
        Me.grb_NETOutput1.Controls.Add(Me.Chk_Washout)
        Me.grb_NETOutput1.Controls.Add(Me.cbo_YawAxisMode)
        Me.grb_NETOutput1.Controls.Add(Me.btn_Rescan)
        Me.grb_NETOutput1.Controls.Add(Me.Label1)
        Me.grb_NETOutput1.Controls.Add(Me.cbo_Devices)
        Me.grb_NETOutput1.Controls.Add(Me.chk_AutoOpenPanel)
        Me.grb_NETOutput1.Controls.Add(Me.btn_OpenPanel)
        Me.grb_NETOutput1.Controls.Add(Me.PictureBox1)
        Me.grb_NETOutput1.Controls.Add(Me.chk_WriteMmf)
        Me.grb_NETOutput1.Controls.Add(Me.cb_StopMS)
        Me.grb_NETOutput1.Controls.Add(Me.cb_StartMS)
        Me.grb_NETOutput1.Controls.Add(Me.cb_PacketRate)
        Me.grb_NETOutput1.Controls.Add(Me.Label230)
        Me.grb_NETOutput1.Controls.Add(Me.Label229)
        Me.grb_NETOutput1.Controls.Add(Me.txt_Port)
        Me.grb_NETOutput1.Controls.Add(Me.txt_IPAddress4)
        Me.grb_NETOutput1.Controls.Add(Me.txt_IPAddress3)
        Me.grb_NETOutput1.Controls.Add(Me.txt_IPAddress2)
        Me.grb_NETOutput1.Controls.Add(Me.txt_IPAddress1)
        Me.grb_NETOutput1.Controls.Add(Me.Label215)
        Me.grb_NETOutput1.Controls.Add(Me.Label217)
        Me.grb_NETOutput1.Controls.Add(Me.Label218)
        Me.grb_NETOutput1.Controls.Add(Me.Label219)
        Me.grb_NETOutput1.Controls.Add(Me.txt_InterfaceOutput)
        Me.grb_NETOutput1.Controls.Add(Me.txt_ShutDownOutput)
        Me.grb_NETOutput1.Controls.Add(Me.txt_StartUpOutput)
        Me.grb_NETOutput1.Controls.Add(Me.Label220)
        Me.grb_NETOutput1.Controls.Add(Me.Label221)
        Me.grb_NETOutput1.Controls.Add(Me.Label223)
        Me.grb_NETOutput1.Controls.Add(Me.rad_HEX)
        Me.grb_NETOutput1.Controls.Add(Me.rad_Decimal)
        Me.grb_NETOutput1.Controls.Add(Me.rad_Binary)
        Me.grb_NETOutput1.Controls.Add(Me.cb_OutputBits)
        Me.grb_NETOutput1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.grb_NETOutput1.ForeColor = System.Drawing.SystemColors.Control
        Me.grb_NETOutput1.Location = New System.Drawing.Point(0, 0)
        Me.grb_NETOutput1.Name = "grb_NETOutput1"
        Me.grb_NETOutput1.Size = New System.Drawing.Size(523, 235)
        Me.grb_NETOutput1.TabIndex = 273
        Me.grb_NETOutput1.TabStop = False
        Me.grb_NETOutput1.Text = "Network Output"
        '
        'Label216
        '
        Me.Label216.AutoSize = True
        Me.Label216.BackColor = System.Drawing.Color.Transparent
        Me.Label216.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label216.Location = New System.Drawing.Point(376, 120)
        Me.Label216.Name = "Label216"
        Me.Label216.Size = New System.Drawing.Size(134, 13)
        Me.Label216.TabIndex = 286
        Me.Label216.Text = "Heading to rotation Speed:"
        '
        'trk_WashoutPower
        '
        Me.trk_WashoutPower.Location = New System.Drawing.Point(405, 65)
        Me.trk_WashoutPower.Maximum = 5
        Me.trk_WashoutPower.Minimum = -5
        Me.trk_WashoutPower.Name = "trk_WashoutPower"
        Me.trk_WashoutPower.Size = New System.Drawing.Size(105, 45)
        Me.trk_WashoutPower.TabIndex = 306
        Me.trk_WashoutPower.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        '
        'trk_Speed
        '
        Me.trk_Speed.Location = New System.Drawing.Point(405, 136)
        Me.trk_Speed.Maximum = 5
        Me.trk_Speed.Minimum = -5
        Me.trk_Speed.Name = "trk_Speed"
        Me.trk_Speed.Size = New System.Drawing.Size(105, 45)
        Me.trk_Speed.TabIndex = 305
        Me.trk_Speed.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        '
        'Chk_Washout
        '
        Me.Chk_Washout.AutoSize = True
        Me.Chk_Washout.Location = New System.Drawing.Point(18, 93)
        Me.Chk_Washout.Name = "Chk_Washout"
        Me.Chk_Washout.Size = New System.Drawing.Size(234, 17)
        Me.Chk_Washout.TabIndex = 304
        Me.Chk_Washout.Text = "Auto Washout to Center (if Yaw Limit active)"
        Me.Chk_Washout.UseVisualStyleBackColor = True
        '
        'cbo_YawAxisMode
        '
        Me.cbo_YawAxisMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo_YawAxisMode.FormattingEnabled = True
        Me.cbo_YawAxisMode.Items.AddRange(New Object() {"Normal", "Smart (Game provides Force)", "Smart (Game provides Heading)"})
        Me.cbo_YawAxisMode.Location = New System.Drawing.Point(102, 67)
        Me.cbo_YawAxisMode.Name = "cbo_YawAxisMode"
        Me.cbo_YawAxisMode.Size = New System.Drawing.Size(180, 21)
        Me.cbo_YawAxisMode.TabIndex = 303
        '
        'btn_Rescan
        '
        Me.btn_Rescan.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btn_Rescan.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Rescan.ForeColor = System.Drawing.Color.Black
        Me.btn_Rescan.Location = New System.Drawing.Point(311, 30)
        Me.btn_Rescan.Name = "btn_Rescan"
        Me.btn_Rescan.Size = New System.Drawing.Size(71, 22)
        Me.btn_Rescan.TabIndex = 302
        Me.btn_Rescan.Text = "Re-scan"
        Me.btn_Rescan.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 301
        Me.Label1.Text = "Device:"
        '
        'cbo_Devices
        '
        Me.cbo_Devices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo_Devices.FormattingEnabled = True
        Me.cbo_Devices.Location = New System.Drawing.Point(18, 32)
        Me.cbo_Devices.Name = "cbo_Devices"
        Me.cbo_Devices.Size = New System.Drawing.Size(287, 21)
        Me.cbo_Devices.TabIndex = 300
        '
        'chk_AutoOpenPanel
        '
        Me.chk_AutoOpenPanel.AutoSize = True
        Me.chk_AutoOpenPanel.Location = New System.Drawing.Point(18, 129)
        Me.chk_AutoOpenPanel.Name = "chk_AutoOpenPanel"
        Me.chk_AutoOpenPanel.Size = New System.Drawing.Size(165, 17)
        Me.chk_AutoOpenPanel.TabIndex = 299
        Me.chk_AutoOpenPanel.Text = "Open Control Panel at startup"
        Me.chk_AutoOpenPanel.UseVisualStyleBackColor = True
        '
        'btn_OpenPanel
        '
        Me.btn_OpenPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btn_OpenPanel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_OpenPanel.ForeColor = System.Drawing.Color.Black
        Me.btn_OpenPanel.Location = New System.Drawing.Point(269, 191)
        Me.btn_OpenPanel.Name = "btn_OpenPanel"
        Me.btn_OpenPanel.Size = New System.Drawing.Size(105, 33)
        Me.btn_OpenPanel.TabIndex = 298
        Me.btn_OpenPanel.Text = "Control Panel"
        Me.btn_OpenPanel.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Yawvr_InterfacePlugin.My.Resources.Resources.logo_yawvr
        Me.PictureBox1.Location = New System.Drawing.Point(12, 150)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(214, 79)
        Me.PictureBox1.TabIndex = 297
        Me.PictureBox1.TabStop = False
        '
        'chk_WriteMmf
        '
        Me.chk_WriteMmf.AutoSize = True
        Me.chk_WriteMmf.Location = New System.Drawing.Point(18, 111)
        Me.chk_WriteMmf.Name = "chk_WriteMmf"
        Me.chk_WriteMmf.Size = New System.Drawing.Size(216, 17)
        Me.chk_WriteMmf.TabIndex = 296
        Me.chk_WriteMmf.Text = "Send motion compensation data (ovrmc)"
        Me.chk_WriteMmf.UseVisualStyleBackColor = True
        '
        'cb_StopMS
        '
        Me.cb_StopMS.BackColor = System.Drawing.SystemColors.Window
        Me.cb_StopMS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_StopMS.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.cb_StopMS.FormattingEnabled = True
        Me.cb_StopMS.IntegralHeight = False
        Me.cb_StopMS.Items.AddRange(New Object() {"-", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "100", "250", "500", "1000", "1800", "2600", "3400", "4200", "5000"})
        Me.cb_StopMS.Location = New System.Drawing.Point(370, 209)
        Me.cb_StopMS.MaxDropDownItems = 10
        Me.cb_StopMS.Name = "cb_StopMS"
        Me.cb_StopMS.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cb_StopMS.Size = New System.Drawing.Size(47, 21)
        Me.cb_StopMS.TabIndex = 278
        Me.cb_StopMS.TabStop = False
        Me.cb_StopMS.Visible = False
        '
        'cb_StartMS
        '
        Me.cb_StartMS.BackColor = System.Drawing.SystemColors.Window
        Me.cb_StartMS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_StartMS.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.cb_StartMS.FormattingEnabled = True
        Me.cb_StartMS.IntegralHeight = False
        Me.cb_StartMS.Items.AddRange(New Object() {"-", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "100", "250", "500", "1000", "1800", "2600", "3400", "4200", "5000"})
        Me.cb_StartMS.Location = New System.Drawing.Point(370, 186)
        Me.cb_StartMS.MaxDropDownItems = 10
        Me.cb_StartMS.Name = "cb_StartMS"
        Me.cb_StartMS.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cb_StartMS.Size = New System.Drawing.Size(47, 21)
        Me.cb_StartMS.TabIndex = 275
        Me.cb_StartMS.TabStop = False
        Me.cb_StartMS.Visible = False
        '
        'cb_PacketRate
        '
        Me.cb_PacketRate.BackColor = System.Drawing.SystemColors.Window
        Me.cb_PacketRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_PacketRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.cb_PacketRate.FormattingEnabled = True
        Me.cb_PacketRate.IntegralHeight = False
        Me.cb_PacketRate.Items.AddRange(New Object() {"-", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50"})
        Me.cb_PacketRate.Location = New System.Drawing.Point(370, 197)
        Me.cb_PacketRate.MaxDropDownItems = 10
        Me.cb_PacketRate.Name = "cb_PacketRate"
        Me.cb_PacketRate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cb_PacketRate.Size = New System.Drawing.Size(47, 21)
        Me.cb_PacketRate.TabIndex = 268
        Me.cb_PacketRate.TabStop = False
        Me.cb_PacketRate.Visible = False
        '
        'btn_Save
        '
        Me.btn_Save.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn_Save.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn_Save.ForeColor = System.Drawing.Color.Black
        Me.btn_Save.Location = New System.Drawing.Point(405, 191)
        Me.btn_Save.Name = "btn_Save"
        Me.btn_Save.Size = New System.Drawing.Size(105, 33)
        Me.btn_Save.TabIndex = 295
        Me.btn_Save.Text = "Save"
        Me.btn_Save.UseVisualStyleBackColor = False
        '
        'Label230
        '
        Me.Label230.AutoSize = True
        Me.Label230.BackColor = System.Drawing.Color.Transparent
        Me.Label230.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label230.Location = New System.Drawing.Point(402, 6)
        Me.Label230.Name = "Label230"
        Me.Label230.Size = New System.Drawing.Size(26, 13)
        Me.Label230.TabIndex = 294
        Me.Label230.Text = "Port"
        Me.Label230.Visible = False
        '
        'Label229
        '
        Me.Label229.AutoSize = True
        Me.Label229.BackColor = System.Drawing.Color.Transparent
        Me.Label229.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label229.Location = New System.Drawing.Point(112, 6)
        Me.Label229.Name = "Label229"
        Me.Label229.Size = New System.Drawing.Size(58, 13)
        Me.Label229.TabIndex = 293
        Me.Label229.Text = "IP Address"
        Me.Label229.Visible = False
        '
        'txt_Port
        '
        Me.txt_Port.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.txt_Port.Location = New System.Drawing.Point(434, 3)
        Me.txt_Port.Name = "txt_Port"
        Me.txt_Port.Size = New System.Drawing.Size(63, 20)
        Me.txt_Port.TabIndex = 292
        Me.txt_Port.Visible = False
        '
        'txt_IPAddress4
        '
        Me.txt_IPAddress4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.txt_IPAddress4.Location = New System.Drawing.Point(345, 4)
        Me.txt_IPAddress4.Name = "txt_IPAddress4"
        Me.txt_IPAddress4.Size = New System.Drawing.Size(41, 20)
        Me.txt_IPAddress4.TabIndex = 291
        Me.txt_IPAddress4.Visible = False
        '
        'txt_IPAddress3
        '
        Me.txt_IPAddress3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.txt_IPAddress3.Location = New System.Drawing.Point(293, 4)
        Me.txt_IPAddress3.Name = "txt_IPAddress3"
        Me.txt_IPAddress3.Size = New System.Drawing.Size(41, 20)
        Me.txt_IPAddress3.TabIndex = 290
        Me.txt_IPAddress3.Visible = False
        '
        'txt_IPAddress2
        '
        Me.txt_IPAddress2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.txt_IPAddress2.Location = New System.Drawing.Point(241, 4)
        Me.txt_IPAddress2.Name = "txt_IPAddress2"
        Me.txt_IPAddress2.Size = New System.Drawing.Size(41, 20)
        Me.txt_IPAddress2.TabIndex = 289
        Me.txt_IPAddress2.Visible = False
        '
        'txt_IPAddress1
        '
        Me.txt_IPAddress1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.txt_IPAddress1.Location = New System.Drawing.Point(189, 4)
        Me.txt_IPAddress1.Name = "txt_IPAddress1"
        Me.txt_IPAddress1.Size = New System.Drawing.Size(41, 20)
        Me.txt_IPAddress1.TabIndex = 288
        Me.txt_IPAddress1.Visible = False
        '
        'Label215
        '
        Me.Label215.AutoSize = True
        Me.Label215.BackColor = System.Drawing.Color.Transparent
        Me.Label215.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label215.Location = New System.Drawing.Point(242, 201)
        Me.Label215.Name = "Label215"
        Me.Label215.Size = New System.Drawing.Size(72, 13)
        Me.Label215.TabIndex = 287
        Me.Label215.Text = "Output - Type"
        Me.Label215.Visible = False
        '
        'Label217
        '
        Me.Label217.AutoSize = True
        Me.Label217.BackColor = System.Drawing.Color.Transparent
        Me.Label217.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label217.Location = New System.Drawing.Point(9, 204)
        Me.Label217.Name = "Label217"
        Me.Label217.Size = New System.Drawing.Size(96, 13)
        Me.Label217.TabIndex = 285
        Me.Label217.Text = "Shutdown - Output"
        Me.Label217.Visible = False
        '
        'Label218
        '
        Me.Label218.AutoSize = True
        Me.Label218.BackColor = System.Drawing.Color.Transparent
        Me.Label218.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label218.Location = New System.Drawing.Point(9, 175)
        Me.Label218.Name = "Label218"
        Me.Label218.Size = New System.Drawing.Size(90, 13)
        Me.Label218.TabIndex = 284
        Me.Label218.Text = "Interface - Output"
        Me.Label218.Visible = False
        '
        'Label219
        '
        Me.Label219.AutoSize = True
        Me.Label219.BackColor = System.Drawing.Color.Transparent
        Me.Label219.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label219.Location = New System.Drawing.Point(402, 49)
        Me.Label219.Name = "Label219"
        Me.Label219.Size = New System.Drawing.Size(85, 13)
        Me.Label219.TabIndex = 267
        Me.Label219.Text = "Washout power:"
        '
        'txt_InterfaceOutput
        '
        Me.txt_InterfaceOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.txt_InterfaceOutput.Location = New System.Drawing.Point(110, 201)
        Me.txt_InterfaceOutput.Name = "txt_InterfaceOutput"
        Me.txt_InterfaceOutput.Size = New System.Drawing.Size(251, 20)
        Me.txt_InterfaceOutput.TabIndex = 283
        Me.txt_InterfaceOutput.TabStop = False
        Me.txt_InterfaceOutput.Visible = False
        '
        'txt_ShutDownOutput
        '
        Me.txt_ShutDownOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.txt_ShutDownOutput.Location = New System.Drawing.Point(110, 209)
        Me.txt_ShutDownOutput.Name = "txt_ShutDownOutput"
        Me.txt_ShutDownOutput.Size = New System.Drawing.Size(251, 20)
        Me.txt_ShutDownOutput.TabIndex = 282
        Me.txt_ShutDownOutput.TabStop = False
        Me.txt_ShutDownOutput.Visible = False
        '
        'txt_StartUpOutput
        '
        Me.txt_StartUpOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.txt_StartUpOutput.Location = New System.Drawing.Point(110, 187)
        Me.txt_StartUpOutput.Name = "txt_StartUpOutput"
        Me.txt_StartUpOutput.Size = New System.Drawing.Size(251, 20)
        Me.txt_StartUpOutput.TabIndex = 281
        Me.txt_StartUpOutput.TabStop = False
        Me.txt_StartUpOutput.Visible = False
        '
        'Label220
        '
        Me.Label220.AutoSize = True
        Me.Label220.BackColor = System.Drawing.Color.Transparent
        Me.Label220.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label220.Location = New System.Drawing.Point(418, 205)
        Me.Label220.Name = "Label220"
        Me.Label220.Size = New System.Drawing.Size(20, 13)
        Me.Label220.TabIndex = 280
        Me.Label220.Text = "ms"
        Me.Label220.Visible = False
        '
        'Label221
        '
        Me.Label221.AutoSize = True
        Me.Label221.BackColor = System.Drawing.Color.Transparent
        Me.Label221.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label221.Location = New System.Drawing.Point(446, 205)
        Me.Label221.Name = "Label221"
        Me.Label221.Size = New System.Drawing.Size(51, 13)
        Me.Label221.TabIndex = 279
        Me.Label221.Text = "HW Stop"
        Me.Label221.Visible = False
        '
        'Label223
        '
        Me.Label223.AutoSize = True
        Me.Label223.BackColor = System.Drawing.Color.Transparent
        Me.Label223.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label223.Location = New System.Drawing.Point(18, 70)
        Me.Label223.Name = "Label223"
        Me.Label223.Size = New System.Drawing.Size(81, 13)
        Me.Label223.TabIndex = 276
        Me.Label223.Text = "Yaw axis mode:"
        '
        'rad_HEX
        '
        Me.rad_HEX.AutoSize = True
        Me.rad_HEX.BackColor = System.Drawing.Color.Transparent
        Me.rad_HEX.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.rad_HEX.Location = New System.Drawing.Point(441, 187)
        Me.rad_HEX.Name = "rad_HEX"
        Me.rad_HEX.Size = New System.Drawing.Size(44, 17)
        Me.rad_HEX.TabIndex = 274
        Me.rad_HEX.Text = "Hex"
        Me.rad_HEX.UseVisualStyleBackColor = False
        Me.rad_HEX.Visible = False
        '
        'rad_Decimal
        '
        Me.rad_Decimal.AutoSize = True
        Me.rad_Decimal.BackColor = System.Drawing.Color.Transparent
        Me.rad_Decimal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.rad_Decimal.Location = New System.Drawing.Point(375, 187)
        Me.rad_Decimal.Name = "rad_Decimal"
        Me.rad_Decimal.Size = New System.Drawing.Size(63, 17)
        Me.rad_Decimal.TabIndex = 273
        Me.rad_Decimal.Text = "Decimal"
        Me.rad_Decimal.UseVisualStyleBackColor = False
        Me.rad_Decimal.Visible = False
        '
        'rad_Binary
        '
        Me.rad_Binary.AutoSize = True
        Me.rad_Binary.BackColor = System.Drawing.Color.Transparent
        Me.rad_Binary.Checked = True
        Me.rad_Binary.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.rad_Binary.Location = New System.Drawing.Point(320, 187)
        Me.rad_Binary.Name = "rad_Binary"
        Me.rad_Binary.Size = New System.Drawing.Size(54, 17)
        Me.rad_Binary.TabIndex = 272
        Me.rad_Binary.TabStop = True
        Me.rad_Binary.Text = "Binary"
        Me.rad_Binary.UseVisualStyleBackColor = False
        Me.rad_Binary.Visible = False
        '
        'cb_OutputBits
        '
        Me.cb_OutputBits.BackColor = System.Drawing.SystemColors.Window
        Me.cb_OutputBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_OutputBits.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.cb_OutputBits.FormattingEnabled = True
        Me.cb_OutputBits.Items.AddRange(New Object() {"-", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.cb_OutputBits.Location = New System.Drawing.Point(35, 202)
        Me.cb_OutputBits.MaxDropDownItems = 25
        Me.cb_OutputBits.Name = "cb_OutputBits"
        Me.cb_OutputBits.Size = New System.Drawing.Size(64, 21)
        Me.cb_OutputBits.TabIndex = 271
        Me.cb_OutputBits.TabStop = False
        Me.cb_OutputBits.Visible = False
        '
        'UserControl_Settings
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.grb_NETOutput1)
        Me.Name = "UserControl_Settings"
        Me.Size = New System.Drawing.Size(523, 235)
        Me.grb_NETOutput1.ResumeLayout(False)
        Me.grb_NETOutput1.PerformLayout()
        CType(Me.trk_WashoutPower, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trk_Speed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grb_NETOutput1 As System.Windows.Forms.GroupBox
    Friend WithEvents cb_StopMS As System.Windows.Forms.ComboBox
    Friend WithEvents cb_StartMS As System.Windows.Forms.ComboBox
    Friend WithEvents cb_PacketRate As System.Windows.Forms.ComboBox
    Friend WithEvents btn_Save As System.Windows.Forms.Button
    Friend WithEvents Label230 As System.Windows.Forms.Label
    Friend WithEvents Label229 As System.Windows.Forms.Label
    Friend WithEvents txt_Port As System.Windows.Forms.TextBox
    Friend WithEvents txt_IPAddress4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_IPAddress3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_IPAddress2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_IPAddress1 As System.Windows.Forms.TextBox
    Friend WithEvents Label215 As System.Windows.Forms.Label
    Friend WithEvents Label216 As System.Windows.Forms.Label
    Friend WithEvents Label217 As System.Windows.Forms.Label
    Friend WithEvents Label218 As System.Windows.Forms.Label
    Friend WithEvents Label219 As System.Windows.Forms.Label
    Friend WithEvents txt_InterfaceOutput As System.Windows.Forms.TextBox
    Friend WithEvents txt_ShutDownOutput As System.Windows.Forms.TextBox
    Friend WithEvents txt_StartUpOutput As System.Windows.Forms.TextBox
    Friend WithEvents Label220 As System.Windows.Forms.Label
    Friend WithEvents Label221 As System.Windows.Forms.Label
    Friend WithEvents Label223 As System.Windows.Forms.Label
    Friend WithEvents rad_HEX As System.Windows.Forms.RadioButton
    Friend WithEvents rad_Decimal As System.Windows.Forms.RadioButton
    Friend WithEvents rad_Binary As System.Windows.Forms.RadioButton
    Friend WithEvents cb_OutputBits As System.Windows.Forms.ComboBox
    Friend WithEvents PictureBox1 As Windows.Forms.PictureBox
    Friend WithEvents chk_WriteMmf As Windows.Forms.CheckBox
    Friend WithEvents btn_OpenPanel As Windows.Forms.Button
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents cbo_Devices As Windows.Forms.ComboBox
    Friend WithEvents chk_AutoOpenPanel As Windows.Forms.CheckBox
    Friend WithEvents btn_Rescan As Windows.Forms.Button
    Friend WithEvents trk_WashoutPower As Windows.Forms.TrackBar
    Friend WithEvents trk_Speed As Windows.Forms.TrackBar
    Friend WithEvents Chk_Washout As Windows.Forms.CheckBox
    Friend WithEvents cbo_YawAxisMode As Windows.Forms.ComboBox
End Class
