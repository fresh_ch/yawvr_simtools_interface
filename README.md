##YawVR Interface Driver for SimTools

**Features:

Very easy to setup: choose the device and its done
Motion compensation support through OVRMC and YawVR Tracker.
Motion scale automatic adjustment to the user's configured limits.

**Installation:

Drop the zip onto the SimTools PluginUpdater. 
In SimTools Game Engine, select "Yaw VR" in the "Interface Settings" section.
enter your IP address and click Save

**Version history:
v0.1: original release
v0.2: added adaptive motion scale
v0.3: added detection of the devices on the network

